<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true"%>

<html>
<head>
<title>Vlasniča upravljačka ploča</title>
<style type="text/css">
.loginMessage {
	text-align: right;
	font-family: serif;
	font-size: 1.2em;
	padding-right: 3%;
}

.errorMessage {
	font-family: serif;
	font-weight: bold;
	font-size: 0.9em;
	color: #FF0000;
	padding: 1em;
}

.buttons {
	padding-left: 90px;
}

form {
	width: 400px;
	padding: 1em;
	border: 1px solid #CCC;
	border-radius: 1em;
}

label {
	display: inline-block;
	width: 90px;
	text-align: right;
}

div+div {
	margin-top: 1em;
}

button {
	margin-left: .5em;
}

input {
	width: 200px;
	box-sizing: border-box;
	border: 1px solid #999;
}
html,body {
 	margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    display: table;
  background: #76b852; /* fallback for old browsers */
  background: -webkit-linear-gradient(right, #76b852, #8DC26F);
  background: -moz-linear-gradient(right, #76b852, #8DC26F);
  background: -o-linear-gradient(right, #76b852, #8DC26F);
  background: linear-gradient(to left, #76b852, #8DC26F);
  font-family: "Roboto", sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;      
}
#ul1 {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333; 
    position: absolute;
    width: 100%;
}

#li1 {
    float: left;
    display:inline; 
    
}
 
#li1 a {
    display: inline-block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

#li1 a:hover:not(.active) {
    background-color: #111;
}

#content {
    display: table-cell;
    text-align: center;
    vertical-align: middle;
}
/* LIST #8 */
#list8 {  }
#list8 ul {  
	
    list-style-type: none;
    margin: 0;
    padding: 0; 
    display:inline-block;}
#list8 ul li { font-family: Impact, Charcoal, sans-serif;font-size:23px; }
#list8 ul li a { display:block; width:500px; height:50px; background-color:#333; border-left:5px solid #222; border-right:5px solid #222; padding-left:10px;
  text-decoration:none; color:#bfe1f1; }
#list8 ul li a:hover {  -moz-transform:rotate(-5deg); -moz-box-shadow:10px 10px 20px #000000;
  -webkit-transform:rotate(-5deg); -webkit-box-shadow:10px 10px 20px #000000;
  transform:rotate(-5deg); box-shadow:10px 10px 20px #000000; }
#uls {
   list-style-type: none;
    margin: 0;
    padding: 0;
    width: 200px;
    background-color: #f1f1f1;
    
}

#lis a,p {
    display: block;
    color: #000;
    padding: 8px 16px;
    text-decoration: none;
}



#lis a:hover:not(.active) {
    background-color: #555;
    color: white;
    cursor:pointer;
}
hr.style-five {
    border: 0;
    height: 0; /* Firefox... */
    box-shadow: 0 0 10px 1px black;
}
hr.style-five:after {  /* Not really supposed to work, but does */
    content: "\00a0";  /* Prevent margin collapse */
}
.active {
    background-color: #1abc9c;
}
.myButton {
	-moz-box-shadow: 3px 4px 0px 0px #899599;
	-webkit-box-shadow: 3px 4px 0px 0px #899599;
	box-shadow: 3px 4px 0px 0px #899599;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #ededed), color-stop(1, #bab1ba));
	background:-moz-linear-gradient(top, #ededed 5%, #bab1ba 100%);
	background:-webkit-linear-gradient(top, #ededed 5%, #bab1ba 100%);
	background:-o-linear-gradient(top, #ededed 5%, #bab1ba 100%);
	background:-ms-linear-gradient(top, #ededed 5%, #bab1ba 100%);
	background:linear-gradient(to bottom, #ededed 5%, #bab1ba 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ededed', endColorstr='#bab1ba',GradientType=0);
	background-color:#ededed;
	-moz-border-radius:15px;
	-webkit-border-radius:15px;
	border-radius:15px;
	border:1px solid #d6bcd6;
	display:inline-block;
	cursor:pointer;
	color:#3a8a9e;
	font-family:Arial;
	font-size:17px;
	padding:7px 25px;
	text-decoration:none;
	text-shadow:0px 1px 0px #e1e2ed;
}
.myButton:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #bab1ba), color-stop(1, #ededed));
	background:-moz-linear-gradient(top, #bab1ba 5%, #ededed 100%);
	background:-webkit-linear-gradient(top, #bab1ba 5%, #ededed 100%);
	background:-o-linear-gradient(top, #bab1ba 5%, #ededed 100%);
	background:-ms-linear-gradient(top, #bab1ba 5%, #ededed 100%);
	background:linear-gradient(to bottom, #bab1ba 5%, #ededed 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#bab1ba', endColorstr='#ededed',GradientType=0);
	background-color:#bab1ba;
}
.myButton:active {
	position:relative;
	top:1px;
}
</style>
</head>

<body>

<ul id="ul1">
  <li id="li1"><a  href="<%=request.getContextPath()%>">Početna</a></li>
  <li id="li1"><a  href="<%=request.getContextPath()%>/servlets/ponuda">Ponuda</a></li>
  ${user.getAuthorizationLevel() == 'OWNER' ? '<li class="active" id="li1"><a  href="">Upravljacka ploca</a></li>' : ""}
  
  <c:choose>
  <c:when test="${empty user}">
  <li id="li1" style="float: right;"><a href="<%=request.getContextPath()%>/servlets/login">Prijava</a></li>  
  </c:when>
  <c:otherwise>
  <li id="li1" style="float: right;" > </a><a href="/rezervacija/servlets/logout">ODJAVA</a> </li>  
  <li id="li1" style="float: right;" > <a class="disabled">Logirani ste kao: <b>${user.firstName} ${user.lastName}</b></li>  
  </c:otherwise>
  </c:choose> 
</ul>

<br><br><br>
<div id="content">
	<p style="font-size:36px;">Pozdrav <b>${user.firstName} ${user.lastName}! </b></p> <hr class="style-five"><br><br>
<div id="list8">
  <ul>  
  <li ><a href="/rezervacija/servlets/owner/editAcommodations">Unos	podataka o smještajnim jedinicama</a></li>
  <li ><a href="/rezervacija/servlets/owner/adminRegistration">Registracija	administratora</a></li>
  <li ><a href="/rezervacija/servlets/owner/editAdmins">Izmjena podataka o administratorima</a></li>
  <li ><a href="/rezervacija/servlets/owner/statistics">Prikaz statistike o rezervacijama</a></li>
  <li ><a ></a></li>
  ${initial == true ? '' : ' <li ><form style="width: 475px" action="/rezervacija/servlets/owner/controlPanel" method="post">
			<button type="submit" class="myButton" 
			name="method" value="Submit">Unesi inicijalne podatke o smještajima</button>
	</form></li>'}  
 
  </ul>
  </div>
	
	
	
	</div>
	
</body>
</html>