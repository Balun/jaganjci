
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true"%>

<html>
<head>
<title>Potvrda e-maila</title>
</head>

<body>
	<div>${user.firstName} ${user.lastName}, Vaša e-mail adresa je
		potvrđena.</div>

	<p>
		<a href="/rezervacija/servlets/login">Prijava.</a>
	</p>

	<p>
		<a href="/rezervacija/servlets/main">Povratak na naslovnu
			stranicu.</a>
	</p>
</body>
</html>