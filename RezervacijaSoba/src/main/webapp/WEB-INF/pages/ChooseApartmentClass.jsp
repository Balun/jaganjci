<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true"%>

<html>
<head>
<title>Unos smještaja</title>
<script>
	$('.message a').click(function() {
		$('form').animate({
			height : "toggle",
			opacity : "toggle"
		}, "slow");
	});
</script>
<style>
@import url(https://fonts.googleapis.com/css?family=Roboto:300);

.login-page {
	width: 360px;
	padding: 8% 0 0;
	margin: auto;
}

.form {
	position: relative;
	z-index: 1;
	background: #FFFFFF;
	max-width: 360px;
	margin: 0 auto 100px;
	padding: 45px;
	text-align: center;
	box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0
		rgba(0, 0, 0, 0.24);
}

.form input {
	font-family: "Roboto", sans-serif;
	outline: 0;
	background: #f2f2f2;
	width: 100%;
	border: 0;
	margin: 0 0 15px;
	padding: 15px;
	box-sizing: border-box;
	font-size: 14px;
}

.form button {
	font-family: "Roboto", sans-serif;
	text-transform: uppercase;
	outline: 0;
	background: #4CAF50;
	width: 100%;
	border: 0;
	padding: 15px;
	color: #FFFFFF;
	font-size: 14px;
	-webkit-transition: all 0.3 ease;
	transition: all 0.3 ease;
	cursor: pointer;
	display: inline;
}

.form button:hover, .form button:active, .form button:focus {
	background: #43A047;
}

.form .message {
	margin: 15px 0 0;
	color: #b3b3b3;
	font-size: 12px;
}

.form .message a {
	color: #4CAF50;
	text-decoration: none;
}

.form .register-form {
	display: none;
}

.container {
	position: relative;
	z-index: 1;
	max-width: 300px;
	margin: 0 auto;
}

.container:before, .container:after {
	content: "";
	display: block;
	clear: both;
}

.container .info {
	margin: 50px auto;
	text-align: center;
}

.container .info h1 {
	margin: 0 0 15px;
	padding: 0;
	font-size: 36px;
	font-weight: 300;
	color: #1a1a1a;
}

.container .info span {
	color: #4d4d4d;
	font-size: 12px;
}

.container .info span a {
	color: #000000;
	text-decoration: none;
}

.container .info span .fa {
	color: #EF3B3A;
}

body {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	background: #76b852; /* fallback for old browsers */
	background: -webkit-linear-gradient(right, #76b852, #8DC26F);
	background: -moz-linear-gradient(right, #76b852, #8DC26F);
	background: -o-linear-gradient(right, #76b852, #8DC26F);
	background: linear-gradient(to left, #76b852, #8DC26F);
	font-family: "Roboto", sans-serif;
	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: grayscale;
}

.errorMessage {
	font-family: serif;
	font-weight: bold;
	font-size: 0.9em;
	color: #FF0000;
	padding: 1em;
	padding-left: 20px;
}

ul {
	list-style-type: none;
	margin: 0;
	padding: 0;
	overflow: hidden;
	background-color: #333;
}

li {
	float: left;
}

li:nth-last-child(1) {
	float: right;
	display: inline-block;
}

li a {
	display: block;
	color: white;
	text-align: center;
	padding: 14px 16px;
	text-decoration: none;
}

li
 
a
:hover
:not
 
(
.active
 
)
{
background-color
:
 
#111
;


}
.active {
	background-color: #1abc9c;
}
</style>
</head>

<body>
	<ul>
		<li><a href="/rezervacija/">Početna</a></li>
		<li><a href="servlets/ponuda">Ponuda</a></li>
		<c:choose>
			<c:when test="${empty user}">
				<li class="active" style="float: right;"><a
					href="<%=request.getContextPath()%>/servlets/login">Prijava</a></li>
			</c:when>
			<c:otherwise>
				<li style="float: right;"></a><a
					href="/rezervacija/servlets/logout">ODJAVA</a></li>
				<li style="float: right;"><a class="disabled">Logirani ste
						kao: <b>${user.firstName} ${user.lastName}</b></li>
				</li>
			</c:otherwise>
		</c:choose>

	</ul>

	<div class="login-page">
		<div class="form">
			<form action="/rezervacija/servlets/owner/editAcommodations"
				method="post">
				<h2>Odaberite klasu smještajne jedinice:</h2>

				<c:if test="${not empty classes}">
					<select name="apClass">
						<c:forEach var="e" items="${classes}">
							<option value="${e.className}">${e.className}</option>
						</c:forEach>
					</select>
				</c:if>

				<div class="buttons">
					<c:if test="${not empty classes}"><button type="submit" name="method" value="Submit">Odaberi</button></c:if>
					<button type="submit" name="method" value="addNew">Dodaj novu klasu smještajnih jedinica</button>
					<button type="submit" name="method" value="Cancel">Odustani</button>
				</div>
			</form>
</body>
</html>