<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true"%>

<html>
<head>
<title>Rezervacija</title>
<script>
	$('.message a').click(function() {
		$('form').animate({
			height : "toggle",
			opacity : "toggle"
		}, "slow");
	});
</script>
<style>
@import url(https://fonts.googleapis.com/css?family=Roboto:300);

.login-page {
	width: 360px;
	padding: 8% 0 0;
	margin: auto;
}

.form {
	position: relative;
	z-index: 1;
	background: #FFFFFF;
	max-width: 360px;
	margin: 0 auto 100px;
	padding: 45px;
	text-align: center;
	box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0
		rgba(0, 0, 0, 0.24);
}

.form input {
	font-family: "Roboto", sans-serif;
	outline: 0;
	background: #f2f2f2;
	width: 100%;
	border: 0;
	margin: 0 0 15px;
	padding: 15px;
	box-sizing: border-box;
	font-size: 14px;
}

.form button {
	font-family: "Roboto", sans-serif;
	text-transform: uppercase;
	outline: 0;
	background: #4CAF50;
	width: 100%;
	border: 0;
	padding: 15px;
	color: #FFFFFF;
	font-size: 14px;
	-webkit-transition: all 0.3 ease;
	transition: all 0.3 ease;
	cursor: pointer;
	display: inline;
}

.form button:hover, .form button:active, .form button:focus {
	background: #43A047;
}

.form .message {
	margin: 15px 0 0;
	color: #b3b3b3;
	font-size: 12px;
}

.form .message a {
	color: #4CAF50;
	text-decoration: none;
}

.form .register-form {
	display: none;
}

.container {
	position: relative;
	z-index: 1;
	max-width: 300px;
	margin: 0 auto;
}

.container:before, .container:after {
	content: "";
	display: block;
	clear: both;
}

.container .info {
	margin: 50px auto;
	text-align: center;
}

.container .info h1 {
	margin: 0 0 15px;
	padding: 0;
	font-size: 36px;
	font-weight: 300;
	color: #1a1a1a;
}

.container .info span {
	color: #4d4d4d;
	font-size: 12px;
}

.container .info span a {
	color: #000000;
	text-decoration: none;
}

.container .info span .fa {
	color: #EF3B3A;
}

body {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	background: #76b852; /* fallback for old browsers */
	background: -webkit-linear-gradient(right, #76b852, #8DC26F);
	background: -moz-linear-gradient(right, #76b852, #8DC26F);
	background: -o-linear-gradient(right, #76b852, #8DC26F);
	background: linear-gradient(to left, #76b852, #8DC26F);
	font-family: "Roboto", sans-serif;
	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: grayscale;
}

.errorMessage {
	font-family: serif;
	font-weight: bold;
	font-size: 0.9em;
	color: #FF0000;
	padding: 1em;
	padding-left: 20px;
}

ul {
	list-style-type: none;
	margin: 0;
	padding: 0;
	overflow: hidden;
	background-color: #333;
}

li {
	float: left;
}

li:nth-last-child(1) {
	float: right;
	display: inline-block;
}

li a {
	display: block;
	color: white;
	text-align: center;
	padding: 14px 16px;
	text-decoration: none;
}

li




 




a








:hover








:not




 




(
.active




 




)
{
background-color








:




 




#111








;
}
.active {
	background-color: #1abc9c;
}
</style>
</head>

<body>
	<ul>
		<li><a href="/rezervacija/">Početna</a></li>
		<li><a href="/rezervacija/servlets/ponuda">Ponuda</a></li>
		<li><a href="/rezervacija/servlets/owner/controlPanel">Upravljacka
				ploca</a></li>
		<c:if test="${userForm.hasError('email')}">
			<div class="errorMessage">
				<c:out value="${userForm.getError('email')}" />
			</div>
		</c:if>
		<c:choose>
			<c:when test="${empty user}">
				<li class="active" style="float: right;"><a
					href="<%=request.getContextPath()%>/servlets/login">Prijava</a></li>
			</c:when>
			<c:otherwise>
				<li style="float: right;"></a><a
					href="/rezervacija/servlets/logout">ODJAVA</a></li>
				<li style="float: right;"><a class="disabled">Logirani ste
						kao: <b>${user.firstName} ${user.lastName}</b></li>
				</li>
			</c:otherwise>
		</c:choose>

	</ul>

	<div class="login-page">
		<div class="form">
			<h3>Izmjena rezervacija</h3>
			<form action="/rezervacija/servlets/admin/editReservations"
				method="post">
				<input type="hidden" name="oldId" value="${regForm.oldId}">

				<div>
					<label for="reservationStart">Datum početka rezervacije:<font
						color="red">*</font></label> <input type="date" name="reservationStart"
						value='<c:out value="${regForm.reservationStart}"/>' size="5"><br>

					<c:if test="${regForm.hasError('reservationStart')}">
						<div class="errorMessage">
							<c:out value="${regForm.getError('reservationStart')}" />
						</div>
					</c:if>
				</div>

				<div>
					<label for="reservationEnd">Datum kraja rezervacije:<font
						color="red">*</font></label> <input type="date" name="reservationEnd"
						value='<c:out value="${regForm.reservationEnd}"/>' size="5"><br>

					<c:if test="${regForm.hasError('reservationEnd')}">
						<div class="errorMessage">
							<c:out value="${regForm.getError('reservationEnd')}" />
						</div>
					</c:if>
				</div>

				<div>
					<label for="adultsNumber">Broj odraslih osoba:</label> <input
						type="number" name="adultsNumber" value="${regForm.adultsNumber}"
						size="5"><br>

					<c:if test="${regForm.hasError('adultsNumber')}">
						<div class="errorMessage">
							<c:out value="${regForm.getError('adultsNumber')}" />
						</div>
					</c:if>
				</div>

				<div>
					<label for="child_0_1">Broj djece od 0 do 1 godine:</label> <input
						type="number" name="child_0_1"
						value='<c:out value="${regForm.child_0_1}"/>' size="5"><br>

					<c:if test="${regForm.hasError('child_0_1')}">
						<div class="errorMessage">
							<c:out value="${regForm.getError('child_0_1')}" />
						</div>
					</c:if>
				</div>

				<div>
					<label for="child_2_7">Broj djece od 2 do 7 godina:</label> <input
						type="number" name="child_2_7"
						value='<c:out value="${regForm.child_2_7}"/>' size="5"><br>

					<c:if test="${regForm.hasError('child_2_7')}">
						<div class="errorMessage">
							<c:out value="${regForm.getError('child_2_7')}" />
						</div>
					</c:if>
				</div>

				<div>
					<label for="child_8_14">Broj djece od 8 do 14 godina:</label> <input
						type="number" name="child_8_14"
						value='<c:out value="${regForm.child_8_14}"/>' size="5"><br>

					<c:if test="${regForm.hasError('child_8_14')}">
						<div class="errorMessage">
							<c:out value="${regForm.getError('child_8_14')}" />
						</div>
					</c:if>
				</div>

				<div>
					<label for="parkingSpace">Parkirno mjesto:</label> <input
						type="checkbox" name="parkingSpace" value="parkingSpace"
						id="parkingSpace"
						<c:if test="${regForm.parkingSpace }">checked</c:if> size="5"><br>
				</div>

				<div>
					<label for="sateliteTv">Satelitska televizija:</label> <input
						type="checkbox" name="sateliteTv" value="sateliteTv"
						id="sateliteTv" <c:if test="${regForm.sateliteTv }">checked</c:if>
						size="5"><br>
				</div>

				<div>
					<label for="wifi">Bežićna internet mreža:</label> <input
						type="checkbox" name="wifi" value="wifi" id="wifi"
						<c:if test="${regForm.wifi }">checked</c:if> size="5"><br>
				</div>

				<div>
					<font color="red">*</font> obavezno polje
				</div>

				<div class="buttons">
					<button type="submit" name="method" value="Submit">Pošalji</button>
					<button type="submit" name="method" value="Cancel">Odustani</button>
				</div>
			</form>
		</div>
	</div>
</body>
</html>