
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true"%>

<html>
<head>
<title>Index</title>
<style type="text/css">
.loginMessage {
	text-align: right;
	font-family: serif;
	font-size: 1.2em;
	padding-right: 3%;
}

.errorMessage {
	font-family: serif;
	font-weight: bold;
	font-size: 0.9em;
	color: #FF0000;
	padding: 1em;
	padding-left: 20px;
}

.buttons {
	padding-left: 90px;
}

form {
	width: 400px;
	padding: 1em;
	border: 1px solid #CCC;
	border-radius: 1em;
}

label {
	display: inline-block;
	width: 90px;
	text-align: right;
}

div+div {
	margin-top: 1em;
}

button {
	margin-left: .5em;
}

input {
	width: 200px;
	box-sizing: border-box;
	border: 1px solid #999;
}
</style>
</head>

<body>
	<c:choose>
		<c:when test="${empty user}">
			<p class="loginMessage">Niste prijavljeni.</p>
		</c:when>
		<c:otherwise>
			<p class="loginMessage">
				${user.firstName} ${user.lastName}, kliknite <a
					href="/rezervacija/servlets/logout">ovdje</a> za odjavu.
			</p>
		</c:otherwise>
	</c:choose>

	<c:if test="${empty user}">
		<h2>Prijava:</h2>
		<form action="/rezervacija/servlets/login" method="post">

			<div>
				<label for="email">E-mail:</label> <input type="text" name="email"
					value='<c:out value="${userForm.email}"/>' size="5"><br>

				<c:if test="${userForm.hasError('email')}">
					<div class="errorMessage">
						<c:out value="${userForm.getError('email')}" />
					</div>
				</c:if>
			</div>

			<div>
				<label for="password">Lozinka:</label> <input type="password"
					name="password" size="5"><br>

				<c:if test="${userForm.hasError('password')}">
					<div class="errorMessage">
						<c:out value="${userForm.getError('password')}" />
					</div>
				</c:if>
			</div>

			<div class="buttons">
				<button type="submit" name="method" value="Submit">Prijava</button>
			</div>
		</form>

	</c:if>
	<p>
		Registrirajte se <a href="/rezervacija/servlets/regRedirect">ovdje</a>.
	</p>
</body>
</html>