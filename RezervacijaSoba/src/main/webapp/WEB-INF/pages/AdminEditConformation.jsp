
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true"%>

<html>
<head>
<title>Mail confirmation</title>
<style type="text/css">
#ul1 {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333; 
    position: absolute;
    width: 100%;
}

#li1 {
    float: left;
    display:inline; 
    
}
 
#li1 a {
    display: inline-block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

#li1 a:hover:not(.active) {
    background-color: #111;
}

html,body {
 	margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    display: table;
  background: #76b852; /* fallback for old browsers */
  background: -webkit-linear-gradient(right, #76b852, #8DC26F);
  background: -moz-linear-gradient(right, #76b852, #8DC26F);
  background: -o-linear-gradient(right, #76b852, #8DC26F);
  background: linear-gradient(to left, #76b852, #8DC26F);
  font-family: "Roboto", sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;      
}
#content {
    display: table-cell;
    text-align: center;
    vertical-align: middle;
}
hr.style-five {
    border: 0;
    height: 0; /* Firefox... */
    box-shadow: 0 0 10px 1px black;
}
hr.style-five:after {  /* Not really supposed to work, but does */
    content: "\00a0";  /* Prevent margin collapse */
}
</style>
</head>

<body>
	<div id="content">
	<p style="font-size:36px;">Administrator <b>${firstName} ${lastName}</b> je uspješno izmijenjen! </b></p> <hr class="style-five"><br><br>
		<p style="font-size:36px; font-family: Impact, Charcoal, sans-serif;">
		<a href="<%=request.getContextPath()%>/servlets/owner/controlPanel">POVRATAK</a></p> 
	
	</div>		
	
</body>
</html>	 