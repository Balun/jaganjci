<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true"%>

<html>
<head>
<title>Vlasniča upravljačka ploča</title>
<style type="text/css">
.loginMessage {
	text-align: right;
	font-family: serif;
	font-size: 1.2em;
	padding-right: 3%;
}

.errorMessage {
	font-family: serif;
	font-weight: bold;
	font-size: 0.9em;
	color: #FF0000;
	padding: 1em;
}

.buttons {
	padding-left: 90px;
}

form {
	width: 400px;
	padding: 1em;
	border: 1px solid #CCC;
	border-radius: 1em;
}

label {
	display: inline-block;
	width: 90px;
	text-align: right;
}

div+div {
	margin-top: 1em;
}

button {
	margin-left: .5em;
}

input {
	width: 200px;
	box-sizing: border-box;
	border: 1px solid #999;
}
html,body {
 	margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    display: table;
  background: #76b852; /* fallback for old browsers */
  background: -webkit-linear-gradient(right, #76b852, #8DC26F);
  background: -moz-linear-gradient(right, #76b852, #8DC26F);
  background: -o-linear-gradient(right, #76b852, #8DC26F);
  background: linear-gradient(to left, #76b852, #8DC26F);
  font-family: "Roboto", sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;      
}
#ul1 {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333; 
    position: absolute;
    width: 100%;
}

#li1 {
    float: left;
    display:inline; 
    
}
 
#li1 a {
    display: inline-block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

#li1 a:hover:not(.active) {
    background-color: #111;
}

#content {
    display: table-cell;
    text-align: center;
    vertical-align: middle;
}
/* LIST #8 */
#list8 {  }
#list8 ul {  
	
    list-style-type: none;
    margin: 0;
    padding: 0; 
    display:inline-block;}
#list8 ul li { font-family: Impact, Charcoal, sans-serif;font-size:23px; }
#list8 ul li a { display:block; width:500px; height:50px; background-color:#333; border-left:5px solid #222; border-right:5px solid #222; padding-left:10px;
  text-decoration:none; color:#bfe1f1; }
#list8 ul li a:hover {  -moz-transform:rotate(-5deg); -moz-box-shadow:10px 10px 20px #000000;
  -webkit-transform:rotate(-5deg); -webkit-box-shadow:10px 10px 20px #000000;
  transform:rotate(-5deg); box-shadow:10px 10px 20px #000000; }
#uls {
   list-style-type: none;
    margin: 0;
    padding: 0;
    width: 200px;
    background-color: #f1f1f1;
    
}

#lis a,p {
    display: block;
    color: #000;
    padding: 8px 16px;
    text-decoration: none;
}



#lis a:hover:not(.active) {
    background-color: #555;
    color: white;
    cursor:pointer;
}
hr.style-five {
    border: 0;
    height: 0; /* Firefox... */
    box-shadow: 0 0 10px 1px black;
}
hr.style-five:after {  /* Not really supposed to work, but does */
    content: "\00a0";  /* Prevent margin collapse */
}
.active {
    background-color: #1abc9c;
}

</style>
</head>

<body>

<ul id="ul1">
  <li id="li1"><a  href="<%=request.getContextPath()%>">Početna</a></li>
  <li id="li1"><a  href="<%=request.getContextPath()%>/servlets/ponuda">Ponuda</a></li>
  ${user.getAuthorizationLevel() == 'OWNER' ? '<li class="active" id="li1"><a  href="">Upravljacka ploca</a></li>' : ""}
  
  <c:choose>
  <c:when test="${empty user}">
  <li id="li1" style="float: right;"><a href="<%=request.getContextPath()%>/servlets/login">Prijava</a></li>  
  </c:when>
  <c:otherwise>
  <li id="li1" style="float: right;" > </a><a href="/rezervacija/servlets/logout">ODJAVA</a> </li>  
  <li id="li1" style="float: right;" > <a class="disabled">Logirani ste kao: <b>${user.firstName} ${user.lastName}</b></li>  
  </c:otherwise>
  </c:choose> 
</ul>

<br><br><br>
<div id="content">
	<p style="font-size:36px;">Pozdrav <b>${user.firstName} ${user.lastName}! </b></p> <hr class="style-five"><br><br>
<div id="list8">
  <ul>  
  <li ><a href="/rezervacija/servlets/admin/displayReservations">Pregled i uređivanje rezervacija</a></li>
  <li ><a ></a></li>
  </ul>
  </div>
	
	</div>
	
</body>
</html>