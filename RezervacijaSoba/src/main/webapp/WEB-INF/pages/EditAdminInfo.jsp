
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true"%>

<html>
<head>
<link rel="stylesheet" type="text/css"
	href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>


<title>Index</title>
<script>
	$('.message a').click(function() {
		$('form').animate({
			height : "toggle",
			opacity : "toggle"
		}, "slow");
	});
</script>
<style>
@import url(https://fonts.googleapis.com/css?family=Roboto:300);

.login-page {
	width: 360px;
	padding: 8% 0 0;
	margin: auto;
}

.form {
	position: relative;
	z-index: 1;
	background: #FFFFFF;
	max-width: 360px;
	margin: 0 auto 100px;
	padding: 45px;
	text-align: center;
	box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0
		rgba(0, 0, 0, 0.24);
}

.form input {
	font-family: "Roboto", sans-serif;
	outline: 0;
	background: #f2f2f2;
	width: 100%;
	border: 0;
	margin: 0 0 15px;
	padding: 15px;
	box-sizing: border-box;
	font-size: 14px;
}

.form button {
	font-family: "Roboto", sans-serif;
	text-transform: uppercase;
	outline: 0;
	background: #4CAF50;
	width: 100%;
	border: 0;
	padding: 15px;
	color: #FFFFFF;
	font-size: 14px;
	-webkit-transition: all 0.3 ease;
	transition: all 0.3 ease;
	cursor: pointer;
}

.form button:hover, .form button:active, .form button:focus {
	background: #43A047;
}

.form .message {
	margin: 15px 0 0;
	color: #b3b3b3;
	font-size: 12px;
}

.form .message a {
	color: #4CAF50;
	text-decoration: none;
}

.form .register-form {
	display: none;
}

.container {
	position: relative;
	z-index: 1;
	max-width: 300px;
	margin: 0 auto;
}

.container:before, .container:after {
	content: "";
	display: block;
	clear: both;
}

.container .info {
	margin: 50px auto;
	text-align: center;
}

.container .info h1 {
	margin: 0 0 15px;
	padding: 0;
	font-size: 36px;
	font-weight: 300;
	color: #1a1a1a;
}

.container .info span {
	color: #4d4d4d;
	font-size: 12px;
}

.container .info span a {
	color: #000000;
	text-decoration: none;
}

.container .info span .fa {
	color: #EF3B3A;
}

body {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	background: #76b852; /* fallback for old browsers */
	background: -webkit-linear-gradient(right, #76b852, #8DC26F);
	background: -moz-linear-gradient(right, #76b852, #8DC26F);
	background: -o-linear-gradient(right, #76b852, #8DC26F);
	background: linear-gradient(to left, #76b852, #8DC26F);
	font-family: "Roboto", sans-serif;
	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: grayscale;
}

.errorMessage {
	font-family: serif;
	font-weight: bold;
	font-size: 0.9em;
	color: #FF0000;
	padding: 1em;
	padding-left: 20px;
}

ul {
	list-style-type: none;
	margin: 0;
	padding: 0;
	overflow: hidden;
	background-color: #333;
}

li {
	float: left;
}

li:nth-last-child(1) {
	float: right;
	display: inline-block;
}

li a {
	display: block;
	color: white;
	text-align: center;
	padding: 14px 16px;
	text-decoration: none;
}

li
 
a
:hover
:not
 
(
.active
 
)
{
background-color
:
 
#111
;


}
.active {
	background-color: #1abc9c;
}
</style>
</head>

<body>
	<ul>
		<li><a href="<%=request.getContextPath()%>">Početna</a></li>
		<li><a href="<%=request.getContextPath()%>/servlets/ponuda">Ponuda</a></li>
		<li id="li1"><a class="active"
			href="<%=request.getContextPath()%>/servlets/owner/controlPanel">Upravljačka
				ploča</a></li>

		<c:choose>
			<c:when test="${empty user}">
				<li class="active" style="float: right;"><a
					href="<%=request.getContextPath()%>/servlets/login">Prijava</a></li>
			</c:when>
			<c:otherwise>
				<li style="float: right;"></a><a
					href="/rezervacija/servlets/logout">ODJAVA</a></li>
				<li style="float: right;"><a class="disabled">Logirani ste
						kao: <b>${user.firstName} ${user.lastName}</b></li>
				</li>
			</c:otherwise>
		</c:choose>

	</ul>

	<div class="login-page">
		<div class="form">

			<form action="/rezervacija/servlets/owner/editAdminInfo"
				method="POST">
				<input type="hidden" name="id" value="${adminId}" />
				<div>
					<label for="firstName">Ime:<font color="red">*</font></label> <input
						type="text" name="firstName"
						value='<c:out value="${regForm.firstName}"/>' size="5"><br>

					<c:if test="${regForm.hasError('firstName')}">
						<div class="errorMessage">
							<c:out value="${regForm.getError('firstName')}" />
						</div>
					</c:if>
				</div>

				<div>
					<label for="lastName">Prezime:<font color="red">*</font></label> <input
						type="text" name="lastName"
						value='<c:out value="${regForm.lastName}"/>' size="5"><br>

					<c:if test="${regForm.hasError('lastName')}">
						<div class="errorMessage">
							<c:out value="${regForm.getError('lastName')}" />
						</div>
					</c:if>
				</div>

				<div>
					<label for="email">Email:<font color="red">*</font></label> <input
						type="email" name="email"
						value='<c:out value="${regForm.email}"/>' size="5"><br>

					<c:if test="${regForm.hasError('email')}">
						<div class="errorMessage">
							<c:out value="${regForm.getError('email')}" />
						</div>
					</c:if>
				</div>

				<div>
					<label for="password">Lozinka:</label> <input
						type="password" name="password" size="5"><br>

					<c:if test="${regForm.hasError('password')}">
						<div class="errorMessage">
							<c:out value="${regForm.getError('password')}" />
						</div>
					</c:if>
				</div>

				<div>
					<label for="passwordConfirmation">Potvrda lozinke:</label> <input type="password"
						name="passwordConfirmation" size="5"><br>

					<c:if test="${regForm.hasError('passwordConfirmation')}">
						<div class="errorMessage">
							<c:out value="${regForm.getError('passwordConfirmation')}" />
						</div>
					</c:if>
				</div>

				<div>
					<label for="streetName">Ime ulice:<font color="red">*</font></label>
					<input type="text" name="streetName"
						value='<c:out value="${regForm.streetName}"/>' size="5"><br>

					<c:if test="${regForm.hasError('streetName')}">
						<div class="errorMessage">
							<c:out value="${regForm.getError('streetName')}" />
						</div>
					</c:if>
				</div>

				<div>
					<label for="streetNumber">Kućni broj:<font color="red">*</font></label>
					<input type="number" name="streetNumber"
						value='<c:out value="${regForm.streetNumber}"/>' size="5"><br>

					<c:if test="${regForm.hasError('streetNumber')}">
						<div class="errorMessage">
							<c:out value="${regForm.getError('streetNumber')}" />
						</div>
					</c:if>
				</div>

				<div>
					<label for="city">Grad:<font color="red">*</font></label> <input
						type="text" name="city" value='<c:out value="${regForm.city}"/>'
						size="5"><br>

					<c:if test="${regForm.hasError('city')}">
						<div class="errorMessage">
							<c:out value="${regForm.getError('city')}" />
						</div>
					</c:if>
				</div>

				<div>
					<label for="country">Država:<font color="red">*</font></label> <input
						type="text" name="country"
						value='<c:out value="${regForm.country}"/>' size="5"><br>

					<c:if test="${regForm.hasError('country')}">
						<div class="errorMessage">
							<c:out value="${regForm.getError('country')}" />
						</div>
					</c:if>
				</div>

				<div>
					<label for="phoneNumber">Broj telefona:</label> <input type="text"
						name="phoneNumber" value='<c:out value="${regForm.phoneNumber}"/>'
						size="5"><br>

					<c:if test="${regForm.hasError('phoneNumber')}">
						<div class="errorMessage">
							<c:out value="${regForm.getError('phoneNumber')}" />
						</div>
					</c:if>
				</div>
				<div>
					<font color="red">*</font> obavezno polje
				</div>

				<div class="buttons">
					<button type="submit" name="method" value="Submit">Pošalji</button>
					<button type="submit" name="method" value="Cancel">Odustani</button>
				</div>
			</form>
		</div>

	</div>
</body>
</html>