
<%@page import="java.util.*"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true"%>

<html>
<head>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
 <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Index</title>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<%List<Integer> galleryPics = new ArrayList<>();
int br = 0;
%>
<c:set var="brojac" value="${0}"/>

<script type="text/javascript">
			var desc = [];
			var minCap = [];
			var maxCap = [];
			var appClass = [];
			var unitV = [];
			var type = [];
			var building = [];
			
		  function searchApartment(name) {
			  //console.log("search " + name);		
			  desc = [];
			  minCap = [];
			  maxCap = [];
			  appClass = [];
			  unitV = [];
			  type = [];
			  building = [];
			  
			$.ajax(
			  { 
				  url: "<%=request.getContextPath()%>/servlets/offer",
				  data: {
					  object: name
				  },
				  dataType: "json",
				  success: function(data) { 
						var apartments = data;  
						//console.log(data);
						var html = "";
						if(apartments.length==0) {
							html = "Nema rezultata..."
						} else {
							for(var i=0; i<apartments.length; i++) {
								if(i > 0) {
									showMyInfo(0);
								}
								
								html += '<input type="button"  class="myButton" onClick="showMyInfo(' + i +')" value="' + apartments[i].className + '">';
								//console.log(apartments[i]);
								desc.push(apartments[i].description);
								minCap.push(apartments[i].minimumCapacity);
								maxCap.push(apartments[i].maximumCapacity);
								appClass.push(apartments[i].className); 
								var v = "";
								if(apartments[i].unitView == 'SEA') {
									v = 'More';
								} else {
									v = 'Park';
								}
								unitV.push(v);
								var t = "";
								if(apartments[i].type == 'APARTMENT') {
									t = 'Apartman';
								} else {
									t = 'Soba';
								}
								type.push(t);
								building.push(apartments[i].building);
								
								
								<%  galleryPics.add(5); %>	<%  galleryPics.add(5); %>
								<%  br++; %>
						
								
							}
						}
						
						$("#apartment").html(html);
						<% request.setAttribute("gList", galleryPics);	%>
						<% request.setAttribute("br", br);	%>
						var htmlgt = ${br};
						$("#gt").html(htmlgt);
				  },
				  error: function(err) {
				       console.log("upao u err -> " + err);
				    }
			  }
			);
		  }
		  		  
		  function showMyInfo (index) {
			  
			  $("#desc").html(desc[index]);
			  $("#minCap").html( minCap[index]);
			  $("#maxCap").html(maxCap[index]);
			  $("#reserve").html('<button  class="myButton" style="display: table;margin: 0 auto;"><a href="/rezervacija/servlets/reservation?apClass=' + appClass[index] + '">Rezerviraj</a></button>');
			  $("#view").html(unitV[index]);
			  $("#type").html(type[index]);
			  $("#building").html(building[index]);
			  
		  }
		  
		  function restartInfo() {
			  $("#desc").html('');
			  $("#minCap").html('');
			  $("#maxCap").html('');
			  
		  }
		  
		  function makeMeActive(id) {
			  document.getElementById("obja").className = "";
			  document.getElementById("objb").className = "";
			  document.getElementById("objc").className = "";
			  document.getElementById("objd").className = "";
			  document.getElementById(id).className = "active2";
		  }
		</script>
		
<style>
body {
  background: #76b852; /* fallback for old browsers */
  background: -webkit-linear-gradient(right, #76b852, #8DC26F);
  background: -moz-linear-gradient(right, #76b852, #8DC26F);
  background: -o-linear-gradient(right, #76b852, #8DC26F);
  background: linear-gradient(to left, #76b852, #8DC26F);
  font-family: "Roboto", sans-serif;     
}

@import url(http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100);
 

div.table-title {
   display: block;
  margin: auto;
  max-width: 600px;
  padding:5px;
  width: 100%;
}

.table-title h3 {
   color: #fafafa;
   font-size: 30px;
   font-weight: 400;
   font-style:normal;
   font-family: "Roboto", helvetica, arial, sans-serif;
   text-shadow: -1px -1px 1px rgba(0, 0, 0, 0.1);
   text-transform:uppercase;
}


/*** Table Styles **/

.table-fill {
  background: white;
  border-radius:3px;
  border-collapse: collapse;
  height: 320px;
  margin: auto;
  max-width: 600px;
  padding:5px;
  width: 100%;
  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);
  animation: float 5s infinite;
}
 
th {
  color:#D5DDE5;;
  background:#1b1e24;
  border-bottom:4px solid #9ea7af;
  border-right: 1px solid #343a45;
  font-size:23px;
  font-weight: 100;
  padding:24px;
  text-align:left;
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
  vertical-align:middle;
}

th:first-child {
  border-top-left-radius:3px;
}
 
th:last-child {
  border-top-right-radius:3px;
  border-right:none;
}
  
tr {
  border-top: 1px solid #C1C3D1;
  border-bottom-: 1px solid #C1C3D1;
  color:#666B85;
  font-size:16px;
  font-weight:normal;
  text-shadow: 0 1px 1px rgba(256, 256, 256, 0.1);
}
 
tr:hover td {
  background:#4E5066;
  color:#FFFFFF;
  border-top: 1px solid #22262e;
  border-bottom: 1px solid #22262e;
}
 
tr:first-child {
  border-top:none;
}

tr:last-child {
  border-bottom:none;
}
 
tr:nth-child(odd) td {
  background:#EBEBEB;
}
 
tr:nth-child(odd):hover td {
  background:#4E5066;
}

tr:last-child td:first-child {
  border-bottom-left-radius:3px;
}
 
tr:last-child td:last-child {
  border-bottom-right-radius:3px;
}
 
td {
  background:#FFFFFF;
  padding:20px;
  text-align:left;
  vertical-align:middle;
  font-weight:300;
  font-size:18px;
  text-shadow: -1px -1px 1px rgba(0, 0, 0, 0.1);
  border-right: 1px solid #C1C3D1;
}

td:last-child {
  border-right: 0px;
}

th.text-left {
  text-align: left;
}

th.text-center {
  text-align: center;
}

th.text-right {
  text-align: right;
}

td.text-left {
  text-align: left;
}

td.text-center {
  text-align: center;
}

td.text-right {
  text-align: right;
}
.myButton2 {
	-moz-box-shadow:inset 0px 1px 0px 0px #a4e271;
	-webkit-box-shadow:inset 0px 1px 0px 0px #a4e271;
	box-shadow:inset 0px 1px 0px 0px #a4e271;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #89c403), color-stop(1, #77a809));
	background:-moz-linear-gradient(top, #89c403 5%, #77a809 100%);
	background:-webkit-linear-gradient(top, #89c403 5%, #77a809 100%);
	background:-o-linear-gradient(top, #89c403 5%, #77a809 100%);
	background:-ms-linear-gradient(top, #89c403 5%, #77a809 100%);
	background:linear-gradient(to bottom, #89c403 5%, #77a809 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#89c403', endColorstr='#77a809',GradientType=0);
	background-color:#89c403;
	-moz-border-radius:6px;
	-webkit-border-radius:6px;
	border-radius:6px;
	border:1px solid #74b807;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:Arial;
	font-size:15px;
	font-weight:bold;
	padding:6px 24px;
	text-decoration:none;
	text-shadow:0px 1px 0px #528009;
}
.myButton2:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #77a809), color-stop(1, #89c403));
	background:-moz-linear-gradient(top, #77a809 5%, #89c403 100%);
	background:-webkit-linear-gradient(top, #77a809 5%, #89c403 100%);
	background:-o-linear-gradient(top, #77a809 5%, #89c403 100%);
	background:-ms-linear-gradient(top, #77a809 5%, #89c403 100%);
	background:linear-gradient(to bottom, #77a809 5%, #89c403 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#77a809', endColorstr='#89c403',GradientType=0);
	background-color:#77a809;
}
.myButton2active {
	position:relative;
	top:1px;
}

html, body {
    margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    
}
#ul1 {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333; 
    width: 100%;
}

#li1 {
    float: left;
    display:inline; 
    
}
 
#li1 a {
    display: inline-block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

#li1 a:hover:not(.active) {
    background-color: #111;
}

.active {
    background-color: #1abc9c;
}
.disabled {
   pointer-events: none;
   cursor: default;
}
#uls {
   list-style-type: none;
    margin: 0;
    padding: 0;
    width: 200px;
    background-color: #b3ffb3;
    display:inline-block;
}

#lis a,p {
    display: block;
    color: black;
    padding: 8px 16px;
    text-decoration: none;
}



#lis a:hover:not(.active) {
    background-color: #777;
    color: white;
    cursor:pointer;
}
.myButton {
	-moz-box-shadow: 3px 4px 0px 0px #899599;
	-webkit-box-shadow: 3px 4px 0px 0px #899599;
	box-shadow: 3px 4px 0px 0px #899599;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #ededed), color-stop(1, #bab1ba));
	background:-moz-linear-gradient(top, #ededed 5%, #bab1ba 100%);
	background:-webkit-linear-gradient(top, #ededed 5%, #bab1ba 100%);
	background:-o-linear-gradient(top, #ededed 5%, #bab1ba 100%);
	background:-ms-linear-gradient(top, #ededed 5%, #bab1ba 100%);
	background:linear-gradient(to bottom, #ededed 5%, #bab1ba 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ededed', endColorstr='#bab1ba',GradientType=0);
	background-color:#ededed;
	-moz-border-radius:15px;
	-webkit-border-radius:15px;
	border-radius:15px;
	border:1px solid #d6bcd6;
	display:inline-block;
	cursor:pointer;
	color:#3a8a9e;
	font-family:Arial;
	font-size:17px;
	padding:7px 25px;
	text-decoration:none;
	text-shadow:0px 1px 0px #e1e2ed;
}
.myButton:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #bab1ba), color-stop(1, #ededed));
	background:-moz-linear-gradient(top, #bab1ba 5%, #ededed 100%);
	background:-webkit-linear-gradient(top, #bab1ba 5%, #ededed 100%);
	background:-o-linear-gradient(top, #bab1ba 5%, #ededed 100%);
	background:-ms-linear-gradient(top, #bab1ba 5%, #ededed 100%);
	background:linear-gradient(to bottom, #bab1ba 5%, #ededed 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#bab1ba', endColorstr='#ededed',GradientType=0);
	background-color:#bab1ba;
}
.myButton:active {
	position:relative;
	top:1px;
}
.active2 {
    background-color: #777;
    color: white;
}
</style>
</head>

<body >
<ul id="ul1">
  <li id="li1"><a  href="<%=request.getContextPath()%>">Početna</a></li>
  <li id="li1"><a class="active" href="<%=request.getContextPath()%>/servlets/ponuda">Ponuda</a></li>
  ${user.getAuthorizationLevel() == 'OWNER' ? '<li id="li1"><a  href="owner/controlPanel">Upravljacka ploca</a></li>' : ""}
  
  <c:choose>
  <c:when test="${empty user}">
  <li id="li1" style="float: right;"><a href="<%=request.getContextPath()%>/servlets/login">Prijava</a></li>  
  </c:when>
  <c:otherwise>
  <li id="li1" style="float: right;" > </a><a href="/rezervacija/servlets/logout">ODJAVA</a> </li>  
  <li id="li1" style="float: right;" > <a class="disabled">Logirani ste kao: <b>${user.firstName} ${user.lastName}</b></li>  
  </c:otherwise>
  </c:choose>
 
</ul>
<br><br><br>
	
  <ul id="uls">
  
  <li id="lis"><a class="active2" id="obja" onClick="searchApartment('A');makeMeActive('obja');restartInfo()">Objekt A</a></li>
  <li id="lis"><a id="objb" onClick="searchApartment('B');makeMeActive('objb');restartInfo()" >Objekt B</a></li>
  <li id="lis"><a id="objc" onClick="searchApartment('C');makeMeActive('objc');restartInfo()" >Objekt C</a></li>
  <li id="lis"><a  id="objd" onClick="searchApartment('D');makeMeActive('objd');restartInfo()" >Objekt D</a></li>
  </ul>
  
  <div id="apartment" style="display: table;
    margin: 0 auto;">&nbsp;</div> <br><br><br>
   
    <table style="display: table;
    margin: 0 auto;">
    <tr>
    <td>OPIS:</td>
    <td>
    <div id="desc" style="display: table;
    margin: 0 auto;">&nbsp;</div> 
    </td>
    </tr>
    <tr>
    <td>Minimalan kapacitet:</td>
    <td>
    <div id="minCap" style="display: table;
    margin: 0 auto;">&nbsp;</div> 
    </td>
    </tr>
    <tr>
    <td>Maksimalan kapacitet:</td>
    <td>
    <div id="maxCap" style="display: table;
    margin: 0 auto;">&nbsp;</div> 
    </td>
    </tr>  
    <tr>
    <td>Pogled:</td>
    <td>
    <div id="view" style="display: table;
    margin: 0 auto;">&nbsp;</div> 
    </td>
    </tr>  
    <tr>
    <td>Tip smještajne jedinice:</td>
    <td>
    <div id="type" style="display: table;
    margin: 0 auto;">&nbsp;</div> 
    </td>
    </tr>   
    <tr>
    <td>Zgrada:</td>
    <td>
    <div id="building" style="display: table;
    margin: 0 auto;">&nbsp;</div> 
    </td>
    </tr>   
    </table>
    <br> <br>
    <div id="reserve" style="display: table;
    margin: 0 auto;">&nbsp;</div> <br> <br>
    
    <!-- #region Jssor Slider Begin -->

    <!-- Generator: Jssor Slider Maker -->
    <!-- Source: http://www.jssor.com/demos/image-gallery.slider -->
    
    <!-- This demo works with jquery library -->

   <script type="text/javascript" src="/rezervacija/js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="/rezervacija/js/jssor.slider.mini.js"></script>
    <!-- use jssor.slider.debug.js instead for debug -->
    <script>
        jQuery(document).ready(function ($) {
            
            var jssor_1_SlideshowTransitions = [
              {$Duration:1200,x:0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:-0.3,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:-0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:0.3,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:0.3,$During:{$Top:[0.3,0.7]},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:-0.3,$SlideOut:true,$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:-0.3,$During:{$Top:[0.3,0.7]},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:0.3,$SlideOut:true,$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:0.3,$Cols:2,$During:{$Left:[0.3,0.7]},$ChessMode:{$Column:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:0.3,$Cols:2,$SlideOut:true,$ChessMode:{$Column:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:0.3,$Rows:2,$During:{$Top:[0.3,0.7]},$ChessMode:{$Row:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:0.3,$Rows:2,$SlideOut:true,$ChessMode:{$Row:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:0.3,$Cols:2,$During:{$Top:[0.3,0.7]},$ChessMode:{$Column:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,y:-0.3,$Cols:2,$SlideOut:true,$ChessMode:{$Column:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:0.3,$Rows:2,$During:{$Left:[0.3,0.7]},$ChessMode:{$Row:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:-0.3,$Rows:2,$SlideOut:true,$ChessMode:{$Row:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:0.3,y:0.3,$Cols:2,$Rows:2,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$ChessMode:{$Column:3,$Row:12},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:0.3,y:0.3,$Cols:2,$Rows:2,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$SlideOut:true,$ChessMode:{$Column:3,$Row:12},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,$Delay:20,$Clip:3,$Assembly:260,$Easing:{$Clip:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,$Delay:20,$Clip:3,$SlideOut:true,$Assembly:260,$Easing:{$Clip:$Jease$.$OutCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,$Delay:20,$Clip:12,$Assembly:260,$Easing:{$Clip:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,$Delay:20,$Clip:12,$SlideOut:true,$Assembly:260,$Easing:{$Clip:$Jease$.$OutCubic,$Opacity:$Jease$.$Linear},$Opacity:2}
            ];
            
            var jssor_1_options = {
              $AutoPlay: true,
              $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $ThumbnailNavigatorOptions: {
                $Class: $JssorThumbnailNavigator$,
                $Cols: 10,
                $SpacingX: 8,
                $SpacingY: 8,
                $Align: 360
              }
            };
            
            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
            
            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizing
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 800);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            //responsive code end
        });
    </script>

    <style>
        
        /* jssor slider arrow navigator skin 05 css */
        /*
        .jssora05l                  (normal)
        .jssora05r                  (normal)
        .jssora05l:hover            (normal mouseover)
        .jssora05r:hover            (normal mouseover)
        .jssora05l.jssora05ldn      (mousedown)
        .jssora05r.jssora05rdn      (mousedown)
        */
        .jssora05l, .jssora05r {
            display: block;
            position: absolute;
            /* size of arrow element */
            width: 40px;
            height: 40px;
            cursor: pointer;
            background: url('/rezervacija/img/a17.png') no-repeat;
            overflow: hidden;
        }
        .jssora05l { background-position: -10px -40px; }
        .jssora05r { background-position: -70px -40px; }
        .jssora05l:hover { background-position: -130px -40px; }
        .jssora05r:hover { background-position: -190px -40px; }
        .jssora05l.jssora05ldn { background-position: -250px -40px; }
        .jssora05r.jssora05rdn { background-position: -310px -40px; }

        /* jssor slider thumbnail navigator skin 01 css */
        /*
        .jssort01 .p            (normal)
        .jssort01 .p:hover      (normal mouseover)
        .jssort01 .p.pav        (active)
        .jssort01 .p.pdn        (mousedown)
        */
        .jssort01 .p {
            position: absolute;
            top: 0;
            left: 0;
            width: 72px;
            height: 72px;
        }
        
        .jssort01 .t {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            border: none;
        }
        
        .jssort01 .w {
            position: absolute;
            top: 0px;
            left: 0px;
            width: 100%;
            height: 100%;
        }
        
        .jssort01 .c {
            position: absolute;
            top: 0px;
            left: 0px;
            width: 68px;
            height: 68px;
            border: #000 2px solid;
            box-sizing: content-box;
            background: url('/rezervacija/img/t01.png') -800px -800px no-repeat;
            _background: none;
        }
        
        .jssort01 .pav .c {
            top: 2px;
            _top: 0px;
            left: 2px;
            _left: 0px;
            width: 68px;
            height: 68px;
            border: #000 0px solid;
            _border: #fff 2px solid;
            background-position: 50% 50%;
        }
        
        .jssort01 .p:hover .c {
            top: 0px;
            left: 0px;
            width: 70px;
            height: 70px;
            border: #fff 1px solid;
            background-position: 50% 50%;
        }
        
        .jssort01 .p.pdn .c {
            background-position: 50% 50%;
            width: 68px;
            height: 68px;
            border: #000 2px solid;
        }
        
        * html .jssort01 .c, * html .jssort01 .pdn .c, * html .jssort01 .pav .c {
            /* ie quirks mode adjust */
            width /**/: 72px;
            height /**/: 72px;
        }
        
    </style>


    <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left:0px; width: 500px; height: 500px; overflow: hidden; visibility: hidden; background-color: #24262e;
      border:3px solid #000;   ">
        <!-- Loading Screen -->
        <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
            <div style="position:absolute;display:block;background:url('/rezervacija/img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
        </div>
        <div id="pics" data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 700px; height: 700px; overflow: hidden;">
              
          
              
             <div data-p="144.50" style="display: none;"> <img data-u="mage" src="<%=request.getContextPath()%>/servlets/image?id=59" />  <img data-u="thumb" src="<%=request.getContextPath()%>/servlets/image?id=59" />  </div>;             
			              <div data-p="144.50" style="display: none;"> <img data-u="mage" src="<%=request.getContextPath()%>/servlets/image?id=60" />  <img data-u="thumb" src="<%=request.getContextPath()%>/servlets/image?id=60" />  </div>;             
			 
			 
       
        
        </div>
        <!-- Thumbnail Navigator -->
        <div data-u="thumbnavigator" class="jssort01" style="position:absolute;left:0px;bottom:0px;width:800px;height:100px;" data-autocenter="1">
            <!-- Thumbnail Item Skin Begin -->
            <div data-u="slides" style="cursor: default;">
                <div data-u="prototype" class="p">
                    <div class="w">
                        <div data-u="thumbnailtemplate" class="t"></div>
                    </div>
                    <div class="c"></div>
                </div>
            </div>
            <!-- Thumbnail Item Skin End -->
        </div>
        <!-- Arrow Navigator -->
        <span data-u="arrowleft" class="jssora05l" style="top:158px;left:8px;width:40px;height:40px;"></span>
        <span data-u="arrowright" class="jssora05r" style="top:158px;right:8px;width:40px;height:40px;"></span>
    </div>

    <!-- #endregion Jssor Slider End -->¸ 
     
    
    <div id="gtt">&nbsp</div>
   
   ${brojac}
  
   <script type="text/javascript">
   searchApartment('A');
  </script>
   
  
</body>

</html>