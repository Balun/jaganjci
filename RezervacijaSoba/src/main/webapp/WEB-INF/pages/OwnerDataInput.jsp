
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true"%>

<html>
<head>
<title>Unos vlasničkih podataka</title>
<style type="text/css">
.loginMessage {
	text-align: right;
	font-family: serif;
	font-size: 1.2em;
	padding-right: 3%;
}

.errorMessage {
	font-family: serif;
	font-weight: bold;
	font-size: 0.9em;
	color: #FF0000;
	padding: 1em;
	padding-left: 20px;
}

.buttons {
	padding-left: 90px;
}

form {
	width: 400px;
	padding: 1em;
	border: 1px solid #CCC;
	border-radius: 1em;
}

label {
	display: inline-block;
	width: 90px;
	text-align: right;
}

div+div {
	margin-top: 1em;
}

button {
	margin-left: .5em;
}

input {
	width: 200px;
	box-sizing: border-box;
	border: 1px solid #999;
}
</style>
</head>

<body>
	<h2 style="display: table; margin: 0 auto;">Unos podataka:</h2>
	<form style="display: table; margin: 0 auto;" action="/rezervacija/servlets/ownerDataInput" method="post">

		<div>
			<label for="email">E-mail:</label> <input type="text" name="email"
				value='<c:out value="${form.email}"/>' size="5"><br>

			<c:if test="${form.hasError('email')}">
				<div class="errorMessage">
					<c:out value="${form.getError('email')}" />
				</div>
			</c:if>
		</div>

		<div>
			<label for="phoneNumber">Broj telefona:</label> <input type="text"
				name="phoneNumber" value='<c:out value="${form.phoneNumber}"/>'
				size="5"><br>

			<c:if test="${form.hasError('phoneNumber')}">
				<div class="errorMessage">
					<c:out value="${form.getError('phoneNumber')}" />
				</div>
			</c:if>
		</div>

		<div class="buttons">
			<button type="submit" name="method" value="Submit">Pošalji</button>
		</div>
	</form>
</body>
</html>