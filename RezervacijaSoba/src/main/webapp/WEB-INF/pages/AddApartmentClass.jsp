<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true"%>

<html>
<head>
<title>Unos nove klase smještaja</title>
<script>
	$('.message a').click(function() {
		$('form').animate({
			height : "toggle",
			opacity : "toggle"
		}, "slow");
	});
</script>
<style>
@import url(https://fonts.googleapis.com/css?family=Roboto:300);

.login-page {
	width: 360px;
	padding: 8% 0 0;
	margin: auto;
}

.form {
	position: relative;
	z-index: 1;
	background: #FFFFFF;
	max-width: 360px;
	margin: 0 auto 100px;
	padding: 45px;
	text-align: center;
	box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0
		rgba(0, 0, 0, 0.24);
}

.form input {
	font-family: "Roboto", sans-serif;
	outline: 0;
	background: #f2f2f2;
	width: 100%;
	border: 0;
	margin: 0 0 15px;
	padding: 15px;
	box-sizing: border-box;
	font-size: 14px;
}

.form button {
	font-family: "Roboto", sans-serif;
	text-transform: uppercase;
	outline: 0;
	background: #4CAF50;
	width: 100%;
	border: 0;
	padding: 15px;
	color: #FFFFFF;
	font-size: 14px;
	-webkit-transition: all 0.3 ease;
	transition: all 0.3 ease;
	cursor: pointer;
	display: inline;
}

.form button:hover, .form button:active, .form button:focus {
	background: #43A047;
}

.form .message {
	margin: 15px 0 0;
	color: #b3b3b3;
	font-size: 12px;
}

.form .message a {
	color: #4CAF50;
	text-decoration: none;
}

.form .register-form {
	display: none;
}

.container {
	position: relative;
	z-index: 1;
	max-width: 300px;
	margin: 0 auto;
}

.container:before, .container:after {
	content: "";
	display: block;
	clear: both;
}

.container .info {
	margin: 50px auto;
	text-align: center;
}

.container .info h1 {
	margin: 0 0 15px;
	padding: 0;
	font-size: 36px;
	font-weight: 300;
	color: #1a1a1a;
}

.container .info span {
	color: #4d4d4d;
	font-size: 12px;
}

.container .info span a {
	color: #000000;
	text-decoration: none;
}

.container .info span .fa {
	color: #EF3B3A;
}

body {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	background: #76b852; /* fallback for old browsers */
	background: -webkit-linear-gradient(right, #76b852, #8DC26F);
	background: -moz-linear-gradient(right, #76b852, #8DC26F);
	background: -o-linear-gradient(right, #76b852, #8DC26F);
	background: linear-gradient(to left, #76b852, #8DC26F);
	font-family: "Roboto", sans-serif;
	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: grayscale;
}

.errorMessage {
	font-family: serif;
	font-weight: bold;
	font-size: 0.9em;
	color: #FF0000;
	padding: 1em;
	padding-left: 20px;
}

ul {
	list-style-type: none;
	margin: 0;
	padding: 0;
	overflow: hidden;
	background-color: #333;
}

li {
	float: left;
}

li:nth-last-child(1) {
	float: right;
	display: inline-block;
}

li a {
	display: block;
	color: white;
	text-align: center;
	padding: 14px 16px;
	text-decoration: none;
}

li

 

a


:hover


:not

 

(
.active

 

)
{
background-color


:

 

#111


;
}
.active {
	background-color: #1abc9c;
}
</style>
</head>

<body>
	<ul>
		<li><a href="/rezervacija/">Početna</a></li>
		<li><a href="/rezervacija/servlets/ponuda">Ponuda</a></li>
		<c:choose>
			<c:when test="${empty user}">
				<li class="active" style="float: right;"><a
					href="<%=request.getContextPath()%>/servlets/login">Prijava</a></li>
			</c:when>
			<c:otherwise>
				<li style="float: right;"></a><a
					href="/rezervacija/servlets/logout">ODJAVA</a></li>
				<li style="float: right;"><a class="disabled">Logirani ste
						kao: <b>${user.firstName} ${user.lastName}</b></li>
				</li>
			</c:otherwise>
		</c:choose>

	</ul>

	<div class="login-page">
		<div class="form">
			<form action="/rezervacija/servlets/owner/addApartmentClass"
				method="post" enctype="multipart/form-data">
				<h2>Unos nove smještajne klase:</h2>
				<div>
					<label>Zgrada:</label> <select name="building">
						<c:forEach var="e" items="${buildings}">
							<option value=${e}>${e}</option>
						</c:forEach>
					</select><br>

					<c:if test="${regForm.hasError('building')}">
						<div class="errorMessage">
							<c:out value="${regForm.getError('building')}" />
						</div>
					</c:if>
				</div>

				<div>
					<label for="label">Oznaka klase:</label> <input type="text"
						name="apClass" value='<c:out value="${regForm.apClass}"/>'
						size="5"><br>

					<c:if test="${regForm.hasError('apClass')}">
						<div class="errorMessage">
							<c:out value="${regForm.getError('apClass')}" />
						</div>
					</c:if>
				</div>

				<div>
					<label for="minimumCapacity">Minimalni Kapacitet: </label> <input
						type="number" name="minimumCapacity"
						value='<c:out value="${regForm.minimumCapacity}"/>' size="5"><br>

					<c:if test="${regForm.hasError('minimumCapacity')}">
						<div class="errorMessage">
							<c:out value="${regForm.getError('minimumCapacity')}" />
						</div>
					</c:if>
				</div>

				<div>
					<label for="maximumCapacity">Maksimalni Kapacitet: </label> <input
						type="number" name="maximumCapacity"
						value='<c:out value="${regForm.maximumCapacity}"/>' size="5"><br>

					<c:if test="${regForm.hasError('maximumCapacity')}">
						<div class="errorMessage">
							<c:out value="${regForm.getError('maximumCapacity')}" />
						</div>
					</c:if>
				</div>

				<div>
					<label for="unitView">Pogled:</label> <select name="unitView">
						<c:forEach var="e" items="${unitViews}">
							<option value=${e.toString()}>${e.toString()}</option>
						</c:forEach>
					</select><br>

					<c:if test="${regForm.hasError('unitView')}">
						<div class="errorMessage">
							<c:out value="${regForm.getError('unitView')}" />
						</div>
					</c:if>
				</div>

				<div>
					<label>Tip smještaja:</label> <select name="type">
						<c:forEach var="e" items="${types}">
							<option value=${e.toString()}>${e.toString()}</option>
						</c:forEach>
					</select><br>

					<c:if test="${regForm.hasError('type')}">
						<div class="errorMessage">
							<c:out value="${regForm.getError('type')}" />
						</div>
					</c:if>
				</div>

				<div>
					<label for="description">Opis jedinice:</label>
					<textarea name="description" rows="4" cols="31"></textarea>
					<br>

					<c:if test="${regForm.hasError('description')}">
						<div class="errorMessage">
							<c:out value="${regForm.getError('description')}" />
						</div>
					</c:if>
				</div>

				<div>
					<label for="images">Slike:</label> <input type="file" name="images"
						multiple />

					<c:if test="${regForm.hasError('images')}">
						<div class="errorMessage">
							<c:out value="${regForm.getError('images')}" />
						</div>
					</c:if>
				</div>

				<div class="buttons">
					<button type="submit" name="method" value="Submit">Pošalji</button>
					<button type="submit" name="method" value="Cancel">Odustani</button>
				</div>
			</form>
</body>
</html>