<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true"%>

<html>
	<title>Potvrda uklanjanja administratora</title>
	
	<p>Administrator uspješno uklonjen.</p>
	<p><a href="/rezervacija/servlets/owner/editAdmins">Povratak na registraciju administratora</a></p>
	<p><a href="/rezervacija/servlets/owner/controlPanel">Povratak na upravljačku ploču vlasnika</a></p>
</html>