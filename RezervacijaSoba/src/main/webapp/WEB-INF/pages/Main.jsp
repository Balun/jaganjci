
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true"%>

<html>
<head>

<style>
p.serif:hover {
     color: #1abc9c;
}

a:hover {
     color: #1abc9c;
}

p.small:hover {
     color: #1abc9c;
}

.blur img {
  -webkit-transition: all 1s ease;
     -moz-transition: all 1s ease;
       -o-transition: all 1s ease;
      -ms-transition: all 1s ease;
          transition: all 1s ease;
}
 
.blur img:hover {
  -webkit-filter: blur(5px);
}
html, body {
    margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    display: table;
     background: url('http://www.chillcover.com/wp-content/uploads/2015/03/Clear-Ocean-Water_HD_chillcover.com_.jpg') no-repeat center center fixed; 
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
}
#content {
    display: table-cell;
    text-align: center;
    vertical-align: middle;
}
p.serif {
    font-family: Impact, Charcoal, sans-serif;
    font-size: 100px;
    color: white;
    display: inline;
    border: 9px solid white;
    border-radius: 9px;
}
ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333; 
    position: absolute;
    width: 100%;
}

li {
    float: left;
}
li:nth-last-child(1) {
    float: right;
    display: inline-block;
}
li a {
    display: inline-block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

li a:hover:not(.active) {
    background-color: #111;
}

.active {
    background-color: #1abc9c;
}
</style>
<title>Dobrodošli</title>
</head>
 <body>
  <ul>
  <li><a class="active"  href="/rezervacija/">Početna</a></li>
  <li><a  href="/rezervacija/servlets/ponuda">Ponuda</a></li>
  <c:if test="${user.getAuthorizationLevel() == 'OWNER'}"><li id="li1"><a  href="/rezervacija/servlets/owner/controlPanel">Upravljačka ploča</a></li></c:if>
  ${user.getAuthorizationLevel() == 'ADMIN' ? '<li id="li1"><a  href="/rezervacija/servlets/admin/controlPanel">Upravljačka ploča</a></li>' : ""}
  <c:choose>
  <c:when test="${empty user}">
  <li style="float: right;"><a href="<%=request.getContextPath()%>/servlets/login">Prijava</a></li>  
  </c:when>
  <c:otherwise>
   <li style="float: right;" > </a><a href="/rezervacija/servlets/logout">ODJAVA</a> </li>  
  <li style="float: right;" > <a class="disabled">Logirani ste kao: <b>${user.firstName} ${user.lastName}</b></li>  
  </li>  
  </c:otherwise>
  </c:choose>
 
</ul>
	 
  <div id="content"> 
      <div><b><font size="7">Kod nas je najljepše!</font></b><div><br>
      <p class="serif">
      DOBRODOŠLI</p><br><br><br>
      <div class ="blur pic">
      <a href = "<%=request.getContextPath()%>/servlets/ponuda">
      <img src="http://www.theclipartwizard.com/signs/51_rightarrow_inv.gif"
      style = "width:90;height:90;"/></a>
 	 </div>
   
</div>

</body>
</html>