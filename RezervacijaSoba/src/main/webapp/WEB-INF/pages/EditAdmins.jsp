<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true"%>

<html>
<head>

<title>Admini</title>
<style>
body {
	background: #76b852; /* fallback for old browsers */
	background: -webkit-linear-gradient(right, #76b852, #8DC26F);
	background: -moz-linear-gradient(right, #76b852, #8DC26F);
	background: -o-linear-gradient(right, #76b852, #8DC26F);
	background: linear-gradient(to left, #76b852, #8DC26F);
	color: #444;
	font: 100%/30px 'Helvetica Neue', helvetica, arial, sans-serif;
	text-shadow: 0 1px 0 #fff;
}

strong {
	font-weight: bold;
}

em {
	font-style: italic;
}

table {
	background: #f5f5f5;
	border-collapse: separate;
	box-shadow: inset 0 1px 0 #fff;
	font-size: 12px;
	line-height: 24px;
	margin: 30px auto;
	text-align: left;
	width: 800px;
}

th {
	background: url(http://jackrugile.com/images/misc/noise-diagonal.png),
		linear-gradient(#777, #444);
	border-left: 1px solid #555;
	border-right: 1px solid #777;
	border-top: 1px solid #555;
	border-bottom: 1px solid #333;
	box-shadow: inset 0 1px 0 #999;
	color: #fff;
	font-weight: bold;
	padding: 10px 15px;
	position: relative;
	text-shadow: 0 1px 0 #000;
}

th:after {
	background: linear-gradient(rgba(255, 255, 255, 0),
		rgba(255, 255, 255, .08));
	content: '';
	display: block;
	height: 25%;
	left: 0;
	margin: 1px 0 0 0;
	position: absolute;
	top: 25%;
	width: 100%;
}

th:first-child {
	border-left: 1px solid #777;
	box-shadow: inset 1px 1px 0 #999;
}

th:last-child {
	box-shadow: inset -1px 1px 0 #999;
}

td {
	border-right: 1px solid #fff;
	border-left: 1px solid #e8e8e8;
	border-top: 1px solid #fff;
	border-bottom: 1px solid #e8e8e8;
	padding: 10px 15px;
	position: relative;
	transition: all 300ms;
}

td:first-child {
	box-shadow: inset 1px 0 0 #fff;
}

td:last-child {
	border-right: 1px solid #e8e8e8;
	box-shadow: inset -1px 0 0 #fff;
}

tr {
	background: url(http://jackrugile.com/images/misc/noise-diagonal.png);
}

tr:nth-child(odd) td {
	background: #f1f1f1
		url(http://jackrugile.com/images/misc/noise-diagonal.png);
}

tr:last-of-type td {
	box-shadow: inset 0 -1px 0 #fff;
}

tr:last-of-type td:first-child {
	box-shadow: inset 1px -1px 0 #fff;
}

tr:last-of-type td:last-child {
	box-shadow: inset -1px -1px 0 #fff;
}

tbody:hover td {
	color: transparent;
	text-shadow: 0 0 3px #aaa;
}

tbody:hover tr:hover td {
	color: #444;
	text-shadow: 0 1px 0 #fff;
}

.loginMessage {
	text-align: right;
	font-family: serif;
	font-size: 1.2em;
	padding-right: 3%;
}

.errorMessage {
	font-family: serif;
	font-weight: bold;
	font-size: 0.9em;
	color: #FF0000;
	padding: 1em;
}

.buttons {
	padding-left: 90px;
}

form {
	width: 400px;
	padding: 1em;
	border: 1px solid #CCC;
	border-radius: 1em;
}

label {
	display: inline-block;
	width: 90px;
	text-align: right;
}

div+div {
	margin-top: 1em;
}

button {
	margin-left: .5em;
}

input {
	width: 200px;
	box-sizing: border-box;
	border: 1px solid #999;
}

html, body {
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	display: table;
	background: #76b852; /* fallback for old browsers */
	background: -webkit-linear-gradient(right, #76b852, #8DC26F);
	background: -moz-linear-gradient(right, #76b852, #8DC26F);
	background: -o-linear-gradient(right, #76b852, #8DC26F);
	background: linear-gradient(to left, #76b852, #8DC26F);
	font-family: "Roboto", sans-serif;
	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: grayscale;
}

#ul1 {
	list-style-type: none;
	margin: 0;
	padding: 0;
	overflow: hidden;
	background-color: #333;
	position: absolute;
	width: 100%;
}

#li1 {
	float: left;
	display: inline;
}

#li1 a {
	display: inline-block;
	color: white;
	text-align: center;
	padding: 14px 16px;
	text-decoration: none;
}

#li1



 



a






:hover






:not



 



(
.active



 



)
{
background-color






:



 



#111






;
}
#content {
	display: table-cell;
	text-align: center;
	vertical-align: middle;
}
/* LIST #8 */
#list8 {
	
}

#list8 ul {
	list-style-type: none;
	margin: 0;
	padding: 0;
	display: inline-block;
}

#list8 ul li {
	font-family: Impact, Charcoal, sans-serif;
	font-size: 23px;
}

#list8 ul li a {
	display: block;
	width: 500px;
	height: 50px;
	background-color: #333;
	border-left: 5px solid #222;
	border-right: 5px solid #222;
	padding-left: 10px;
	text-decoration: none;
	color: #bfe1f1;
}

#list8 ul li a:hover {
	-moz-transform: rotate(-5deg);
	-moz-box-shadow: 10px 10px 20px #000000;
	-webkit-transform: rotate(-5deg);
	-webkit-box-shadow: 10px 10px 20px #000000;
	transform: rotate(-5deg);
	box-shadow: 10px 10px 20px #000000;
}

#uls {
	list-style-type: none;
	margin: 0;
	padding: 0;
	width: 200px;
	background-color: #f1f1f1;
}

#lis a, p {
	display: block;
	color: #000;
	padding: 8px 16px;
	text-decoration: none;
}

#lis



 



a






:hover






:not



 



(
.active



 



)
{
background-color






:



 



#555






;
color






:



 



white






;
cursor






:



 



pointer






;
}
hr.style-five {
	border: 0;
	height: 0; /* Firefox... */
	box-shadow: 0 0 10px 1px black;
}

hr.style-five:after { /* Not really supposed to work, but does */
	content: "\00a0"; /* Prevent margin collapse */
}

.active {
	background-color: #1abc9c;
}
</style>

</head>
<body>

	<ul id="ul1">
		<li id="li1"><a href="<%=request.getContextPath()%>">Početna</a></li>
		<li id="li1"><a
			href="<%=request.getContextPath()%>/servlets/ponuda">Ponuda</a></li>
		${user.getAuthorizationLevel() == 'OWNER' ? '<li class="active" id="li1"><a  href="/rezervacija/servlets/owner">Upravljacka ploca</a></li>' : ""}

		<c:choose>
			<c:when test="${empty user}">
				<li id="li1" style="float: right;"><a
					href="<%=request.getContextPath()%>/servlets/login">Prijava</a></li>
			</c:when>
			<c:otherwise>
				<li id="li1" style="float: right;"></a><a
					href="/rezervacija/servlets/logout">ODJAVA</a></li>
				<li id="li1" style="float: right;"><a class="disabled">Logirani
						ste kao: <b>${user.firstName} ${user.lastName}</b></li>
			</c:otherwise>
		</c:choose>
	</ul>

	<br>
	<br>
	<br>

	<table>

		<thead>
			<tr>
				<th><font size="5">Ime</font></th>
				<th><font size="5">Prezime</font></th>
				<th><font size="5">E-mail</font></th>
				<th><img src="/rezervacija/icons/edit.png" width="30"
					height="30" /></th>
				<th><img src="/rezervacija/icons/delete.png" width="30"
					height="30" /></th>
			</tr>
		</thead>
		<c:forEach var="admin" items="${admins}">

			<tr>
				<td>${admin.firstName}</td>
				<td>${admin.lastName}</td>
				<td>${admin.email}</td>
				<td><a
					href="<%=request.getContextPath()%>/servlets/owner/editAdminInfo?adminEmail=${admin.email}">Uredi</a></td>
				<td><a
					href="<%=request.getContextPath()%>/servlets/owner/deleteAdmin?email=${admin.email}">Ukloni</a></td>
			</tr>

		</c:forEach>


	</table>
</body>
</html>