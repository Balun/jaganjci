package hr.fer.opp.projekt.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.opp.projekt.dao.DAOProvider;
import hr.fer.opp.projekt.model.AuthorizationLevel;
import hr.fer.opp.projekt.model.Reservation;
import hr.fer.opp.projekt.web.ServletsUtility;
import hr.fer.opp.projekt.web.servlets.form.ReservationForm;

@WebServlet("/servlets/admin/editReservation")
public class EditReservationServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		if (ServletsUtility.allowOnly(req, resp, AuthorizationLevel.ADMIN)) {
			return;
		}

		String idParam = req.getParameter("id");
		if (idParam == null) {
			ServletsUtility.sendError(req, resp, "Nedostaje parametar \"id\".");
			return;
		}
		Long id;
		try {
			id = Long.parseLong(idParam);
		} catch (NumberFormatException e) {
			ServletsUtility.sendError(req, resp,
					"Parametar id mora biti broj.");
			return;
		}
		Reservation res = DAOProvider.getDAO().getReservation(id);
		if (res == null) {
			ServletsUtility.sendError(req, resp,
					"Ne postoji tražena rezervacija.");
			return;
		}

		ReservationForm form = new ReservationForm(res.getClient());
		form.setOldId(id.toString());
		req.setAttribute("regForm", form);

		req.getRequestDispatcher("/WEB-INF/pages/EditReservationInfo.jsp")
				.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		if (ServletsUtility.allowOnly(req, resp, AuthorizationLevel.ADMIN)) {
			return;
		}

		String regMethod = req.getParameter("method");
		if (!"Submit".equals(regMethod)) {
			resp.sendRedirect(req.getServletContext().getContextPath()
					+ "/servlets/admin/DisplayReservations.jsp");
			return;
		}

		Reservation res = null;
		try {
			res = DAOProvider.getDAO()
					.getReservation(Long.parseLong(req.getParameter("oldId")));
		} catch (NumberFormatException e) {
			ServletsUtility.sendError(req, resp,
					"Parametar \"id\" mora biti broj.");
			return;
		}
		if (res == null) {
			ServletsUtility.sendError(req, resp,
					"Ne postoji rezervacija sa traženim ID-om.");
			return;
		}

		ReservationForm form = new ReservationForm(res.getClient());
		form.fillFromHttpRequest(req);
		DAOProvider.getDAO().removeObject(res);
		synchronized (ReservationForm.syncObject) {

			form.validate();

			if (!form.hasErrors()) {
				Reservation newRes = new Reservation();
				form.fillReservationData(newRes);
				DAOProvider.getDAO().addObject(newRes);
				ServletsUtility.sendInfo(req, resp, "Status rezervacije",
						"Uspješno ste izmjenili rezervaciju.");
			} else {
				DAOProvider.getDAO().addObject(res);
				req.getRequestDispatcher(
						"/WEB-INF/pages/EditReservationInfo.jsp")
						.forward(req, resp);
			}
		}
	}
}
