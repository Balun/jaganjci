package hr.fer.opp.projekt.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.opp.projekt.dao.DAOProvider;
import hr.fer.opp.projekt.model.User;

@WebServlet("/servlets/ownerLogin")
public class OwnerLoginServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if (req.getServletContext().getAttribute("ownerConfirmed") != null) {
			resp.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		req.getRequestDispatcher("/WEB-INF/pages/OwnerLogin.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if (req.getServletContext().getAttribute("ownerConfirmed") != null) {
			resp.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		
		String password = req.getParameter("password");
		User owner = DAOProvider.getDAO().getOwner();
		if (password == null || !owner.validatePassword(password)) {
			req.setAttribute("wrongPassword", true);
			req.getRequestDispatcher("/WEB-INF/pages/OwnerLogin.jsp").forward(req, resp);

		} else {
			req.getSession().setAttribute("userID", owner.getId());
			resp.sendRedirect(req.getServletContext().getContextPath() + "/servlets/ownerDataInput");
		}
	}

}
