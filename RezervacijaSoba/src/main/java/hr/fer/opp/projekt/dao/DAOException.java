package hr.fer.opp.projekt.dao;

/**
 * Exception thrown when error occurs while DAO accesses the data.
 * 
 * @author Borna Zeba
 * @version 1.0
 *
 */
public class DAOException extends RuntimeException {

	/**
	 * Version ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new DAO exception with {@code null} as its description message.
	 */
	public DAOException() {
		super();
	}

	/**
	 * Constructs a new DAO exception with the given parameters.
	 * 
	 * @param message
	 *            the detail message
	 * @param cause
	 *            the cause
	 * @param enableSuppression
	 *            whether or not suppression is enabled or disabled
	 * @param writableStackTrace
	 *            whether or not the stack trace should be writable
	 */
	public DAOException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * Constructs a new DAO exception with the given parameters.
	 * 
	 * @param message
	 *            the detail message
	 * @param cause
	 *            the cause
	 */
	public DAOException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructs a new DAO exception with the given parameters.
	 * 
	 * @param message
	 *            the detail message
	 */
	public DAOException(String message) {
		super(message);
	}

	/**
	 * Constructs a new DAO exception with the given parameters.
	 * 
	 * @param cause
	 *            the cause
	 */
	public DAOException(Throwable cause) {
		super(cause);
	}
}
