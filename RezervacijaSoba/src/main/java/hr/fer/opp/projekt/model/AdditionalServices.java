package hr.fer.opp.projekt.model;

public enum AdditionalServices {
	PARKING_SPACE("Parkirno mjesto"), WIFI("Wi-Fi"), SATELLITE_TV(
			"Satelitska televizija");

	private String name;

	private AdditionalServices(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}
}
