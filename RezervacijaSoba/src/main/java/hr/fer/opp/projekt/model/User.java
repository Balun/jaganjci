package hr.fer.opp.projekt.model;

import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import hr.fer.opp.projekt.web.HashUtility;

@Entity
@Table(name = "users")
@NamedQueries({
	@NamedQuery(name = "User.byEmail", query = "SELECT u FROM User AS u WHERE u.email = :email"), 
	@NamedQuery(name = "User.owner", query = "SELECT u FROM User AS u WHERE u.authorizationLevel = hr.fer.opp.projekt.model.AuthorizationLevel.OWNER"),
	@NamedQuery(name = "User.byAuthorizationLevel", query = "SELECT u FROM User AS u WHERE u.authorizationLevel = :authorizationLevel")
	})
public class User {

	private static final int SALT_LENGTH = 64;

	@Id
	@GeneratedValue
	private Long id;

	/**
	 * User's first name.
	 */
	@Column(length = 40, nullable = false)
	private String firstName;

	/**
	 * User's last name.
	 */
	@Column(length = 50, nullable = false)
	private String lastName;

	@Column(length = 50, nullable = false)
	private String streetName;

	@Column(nullable = false)
	private Integer streetNumber;

	@Column(length = 30, nullable = false)
	private String city;

	@Column(length = 30, nullable = false)
	private String country;

	@Column(length = 80, nullable = false, unique = true)
	private String email;

	@Column(length = 65, nullable = false)
	private String passwordHash;

	@Column(length = SALT_LENGTH, nullable = false)
	private String salt;

	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	private AuthorizationLevel authorizationLevel;
	
	@Column(length = 20)
	private String phoneNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private Date registrationDate;
	
	@Column
	private boolean confirmedEmail;
	
	@Column
	private boolean confirmedFirstReservation;

	@OneToMany(mappedBy = "client", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST, orphanRemoval = true)
	private List<Reservation> reservations;

	public User() {
		SecureRandom random = new SecureRandom();
		byte[] bytes = new byte[SALT_LENGTH];
		random.nextBytes(bytes);
		salt = new String(bytes, StandardCharsets.UTF_8);

		authorizationLevel = AuthorizationLevel.USER;
		registrationDate = new Date();
		confirmedFirstReservation = false;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public Integer getStreetNumber() {
		return streetNumber;
	}

	public void setStreetNumber(Integer streetNumber) {
		this.streetNumber = streetNumber;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPasswordHash() {
		return passwordHash;
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}


	public AuthorizationLevel getAuthorizationLevel() {
		return authorizationLevel;
	}

	public void setAuthorizationLevel(AuthorizationLevel authorizationLevel) {
		this.authorizationLevel = authorizationLevel;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public List<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}
	
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public void setPassword(String password) {
		passwordHash = HashUtility.hashValueOf(password + salt);
	}
	
	public boolean validatePassword(String password) {
		return HashUtility.hashValueOf(password + salt).equals(passwordHash);
	}

	public boolean isConfirmedEmail() {
		return confirmedEmail;
	}

	public void setConfirmedEmail(boolean confirmedEmail) {
		this.confirmedEmail = confirmedEmail;
	}
	
	public boolean isConfirmedFirstReservation() {
		return confirmedFirstReservation;
	}

	public void setConfirmedFirstReservation(boolean confirmedFirstReservation) {
		this.confirmedFirstReservation = confirmedFirstReservation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		return true;
	}
}
