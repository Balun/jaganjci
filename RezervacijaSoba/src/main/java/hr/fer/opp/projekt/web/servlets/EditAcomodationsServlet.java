package hr.fer.opp.projekt.web.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.opp.projekt.dao.DAOProvider;
import hr.fer.opp.projekt.model.AcommodationType;
import hr.fer.opp.projekt.model.ApartmentClass;
import hr.fer.opp.projekt.model.AuthorizationLevel;
import hr.fer.opp.projekt.model.BuildingType;
import hr.fer.opp.projekt.model.LandscapeView;
import hr.fer.opp.projekt.web.ServletsUtility;

@WebServlet("/servlets/owner/editAcommodations")
@MultipartConfig
public class EditAcomodationsServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if (ServletsUtility.allowOnly(req, resp, AuthorizationLevel.OWNER)) {
			return;
		}

		List<ApartmentClass> apClasses = DAOProvider.getDAO().getClasses();
		req.setAttribute("classes", apClasses);
		req.getRequestDispatcher("/WEB-INF/pages/ChooseApartmentClass.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if (ServletsUtility.allowOnly(req, resp, AuthorizationLevel.OWNER)) {
			return;
		}

		String regMethod = req.getParameter("method");
		if ("Cancel".equals(regMethod)) {
			resp.sendRedirect(req.getServletContext().getContextPath() + "/servlets/owner/controlPanel");
			return;

		}

		if ("Submit".equalsIgnoreCase(regMethod)) {
			String apClassParam = req.getParameter("apClass");
			if (apClassParam == null) {
				ServletsUtility.sendError(req, resp, "Nema parametra!");
				return;
			}

			req.setAttribute("apClass", apClassParam);
			req.getRequestDispatcher("/WEB-INF/pages/AddAcommodationUnit.jsp").forward(req, resp);

		} else if ("addNew".equalsIgnoreCase(regMethod)) {

			fillRequest(req);
			req.getRequestDispatcher("/WEB-INF/pages/AddApartmentClass.jsp").forward(req, resp);

		} else {
			ServletsUtility.sendError(req, resp, "Netočna metoda!");
			return;
		}
	}

	private void fillRequest(HttpServletRequest req) {
		req.setAttribute("buildings", BuildingType.values());
		req.setAttribute("unitViews", LandscapeView.values());
		req.setAttribute("types", AcommodationType.values());
	}

}
