package hr.fer.opp.projekt.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "apartment_classes")
@NamedQueries({
		@NamedQuery(name = "ApartmentClass.byBuilding", query = "SELECT c FROM ApartmentClass AS c WHERE c.building = :building"),
		@NamedQuery(name = "ApartmentClass.byName", query = "SELECT c FROM ApartmentClass AS c WHERE c.className = :className"),
		@NamedQuery(name = "ApartmentClass.all", query = "SELECT c FROM ApartmentClass AS c")})

public class ApartmentClass {

	@Id
	@GeneratedValue
	private Long id;

	@Column(length = 20, nullable = false, unique = true)
	private String className;

	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	private BuildingType building;

	@Column(nullable = false)
	private Integer minimumCapacity;

	@Column(nullable = false)
	private Integer maximumCapacity;

	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	private LandscapeView unitView;
	
	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	private AcommodationType type;
	
	@Column(length = 2000)
	private String description;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST, orphanRemoval = true)
	private List<Image> images;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST, orphanRemoval = true)
	private List<AcommodationUnit> units;

	public ApartmentClass() {
	}

	public ApartmentClass(String className, BuildingType building, Integer minimumCapacity, Integer maximumCapacity,
			LandscapeView unitView, AcommodationType type, String description, List<Image> images) {
		this(className, building, minimumCapacity, maximumCapacity, unitView, type, description, images, null);
	}

	public ApartmentClass(String className, BuildingType building, Integer minimumCapacity, Integer maximumCapacity,
			LandscapeView unitView, AcommodationType type, String description, List<Image> images, List<AcommodationUnit> units) {
		super();
		this.className = className;
		this.building = building;
		this.minimumCapacity = minimumCapacity;
		this.maximumCapacity = maximumCapacity;
		this.unitView = unitView;
		this.type = type;
		this.description = description;
		this.images = images;
		this.units = units;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Integer getMinimumCapacity() {
		return minimumCapacity;
	}

	public void setMinimumCapacity(Integer minimumCapacity) {
		this.minimumCapacity = minimumCapacity;
	}

	public Integer getMaximumCapacity() {
		return maximumCapacity;
	}

	public void setMaximumCapacity(Integer maximumCapacity) {
		this.maximumCapacity = maximumCapacity;
	}

	public LandscapeView getUnitView() {
		return unitView;
	}

	public void setUnitView(LandscapeView unitView) {
		this.unitView = unitView;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}

	public List<AcommodationUnit> getUnits() {
		return units;
	}

	public void setUnits(List<AcommodationUnit> units) {
		this.units = units;
	}

	public BuildingType getBuilding() {
		return building;
	}

	public void setBuilding(BuildingType building) {
		this.building = building;
	}

	public AcommodationType getType() {
		return type;
	}

	public void setType(AcommodationType type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((className == null) ? 0 : className.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ApartmentClass other = (ApartmentClass) obj;
		if (className == null) {
			if (other.className != null)
				return false;
		} else if (!className.equals(other.className))
			return false;
		return true;
	}

}
