package hr.fer.opp.projekt.web.servlets.form;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import hr.fer.opp.projekt.dao.DAOProvider;
import hr.fer.opp.projekt.model.User;

/**
 * Class representing the form used for registering the blog users.
 * 
 * @author Borna Zeba
 * @version 1.0
 *
 */
public class RegisterUserForm extends AbstractForm {

	/**
	 * User's first name.
	 */
	private String firstName;

	/**
	 * User's last name.
	 */
	private String lastName;

	/**
	 * User's email.
	 */
	private String email;

	/**
	 * User's password.
	 */
	private String password;

	/**
	 * Confirmation of the provided password.
	 */
	private String passwordConfirmation;

	private String streetName;

	private String streetNumber;

	private String city;

	private String country;

	private String phoneNumber;

	@Override
	public void fillFromHttpRequest(HttpServletRequest req) {
		this.firstName = prepare(req.getParameter("firstName"));
		this.lastName = prepare(req.getParameter("lastName"));
		this.email = prepare(req.getParameter("email"));
		this.password = prepareWithoutTrim(req.getParameter("password"));
		this.passwordConfirmation = prepareWithoutTrim(
				req.getParameter("passwordConfirmation"));
		this.streetName = prepare(req.getParameter("streetName"));
		this.streetNumber = prepare(req.getParameter("streetNumber"));
		this.city = prepare(req.getParameter("city"));
		this.country = prepare(req.getParameter("country"));
		this.phoneNumber = prepare(req.getParameter("phoneNumber"));
	}

	/**
	 * Fills the given user with the data from this form.
	 * 
	 * @param user
	 *            the user object to fill
	 */
	public void fillUserData(User user) {
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setEmail(email);
		if (!password.isEmpty()) {	
			user.setPassword(password);
		}
		user.setCity(city);
		user.setCountry(country);
		user.setStreetName(streetName);
		user.setStreetNumber(Integer.parseInt(streetNumber));
		if (!phoneNumber.isEmpty()) {
			user.setPhoneNumber(phoneNumber);
		}
		user.setConfirmedEmail(false);
	}

	/**
	 * Fills the fields of this form with the data from the given user.
	 * 
	 * @param user
	 *            the user whose data will be filled in this
	 */
	public void fillFromUserData(User user) {
		this.firstName = user.getFirstName();
		this.lastName = user.getLastName();
		this.email = user.getEmail();
		this.password = "";
		this.passwordConfirmation = "";
		this.streetName = user.getStreetName();
		this.streetNumber = user.getStreetNumber().toString();
		this.city = user.getCity();
		this.country = user.getCountry();
		this.phoneNumber = user.getPhoneNumber();
	}

	@Override
	public void validate() {
		errors.clear();

		if (email.isEmpty() || !isEmailValid(email)) {
			errors.put("email", "Krivi format e-mail adrese.");

		} else if (DAOProvider.getDAO().getUser(email) != null) {
			errors.put("email", "Već postoji korisnik s tom e-mail adresom.");
		}
		if (password.isEmpty()) {
			errors.put("password", "Morate upisati lozinku.");
		}
		if (password.length() < 6) {
			errors.put("password", "Lozinka mora imati barem 6 znakova.");
		}
		if (!password.equals(passwordConfirmation)) {
			errors.put("password", "Lozinke se ne podudaraju.");
		}
		if (firstName.isEmpty()) {
			errors.put("firstName", "Morate upisati ime.");
		}
		if (lastName.isEmpty()) {
			errors.put("lastName", "Morate upisati prezime.");
		}
		if (streetName.isEmpty()) {
			errors.put("streetName", "Morate upisati ime ulice.");
		}
		if (streetNumber.isEmpty()) {
			errors.put("streetNumber", "Morate upisati kućni broj.");
		}
		try {
			Integer.parseInt(streetNumber);
		} catch (NumberFormatException e) {
			errors.put("streetNumber", "Kućni broj mora biti broj.");
		}
		if (city.isEmpty()) {
			errors.put("city", "Morate upisati grad.");
		}
		if (country.isEmpty()) {
			errors.put("country", "Morate upisati državu.");
		}
	}

	public void validateEdit() {
		errors.clear();

		if (email.isEmpty() || !isEmailValid(email)) {
			errors.put("email", "Krivi format e-mail adrese.");

		}
		if (!password.isEmpty()) {
			if (password.length() < 6) {
				errors.put("password", "Lozinka mora imati barem 6 znakova.");
			}
			if (!password.equals(passwordConfirmation)) {
				errors.put("password", "Lozinke se ne podudaraju.");
			}
		}
		if (firstName.isEmpty()) {
			errors.put("firstName", "Morate upisati ime.");
		}
		if (lastName.isEmpty()) {
			errors.put("lastName", "Morate upisati prezime.");
		}
		if (streetName.isEmpty()) {
			errors.put("streetName", "Morate upisati ime ulice.");
		}
		if (streetNumber.isEmpty()) {
			errors.put("streetNumber", "Morate upisati kućni broj.");
		}
		try {
			Integer.parseInt(streetNumber);
		} catch (NumberFormatException e) {
			errors.put("streetNumber", "Kućni broj mora biti broj.");
		}
		if (city.isEmpty()) {
			errors.put("city", "Morate upisati grad.");
		}
		if (country.isEmpty()) {
			errors.put("country", "Morate upisati državu.");
		}
	}

	/**
	 * Returns {@code true} if the given email is valid, {@code false}
	 * otherwise.
	 * 
	 * @param email
	 *            the email to check
	 * @return {@code true} if the given mail is valid
	 */
	public static boolean isEmailValid(String email) {
		Pattern pattern = Pattern.compile(".+@.+\\..+");
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	public String getPasswordConfirmation() {
		return passwordConfirmation;
	}

	public String getStreetName() {
		return streetName;
	}

	public String getStreetNumber() {
		return streetNumber;
	}

	public String getCity() {
		return city;
	}

	public String getCountry() {
		return country;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

}
