package hr.fer.opp.projekt.web;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import java.util.function.BiFunction;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.opp.projekt.dao.DAOProvider;
import hr.fer.opp.projekt.model.AcommodationType;
import hr.fer.opp.projekt.model.AcommodationUnit;
import hr.fer.opp.projekt.model.AdditionalServices;
import hr.fer.opp.projekt.model.ApartmentClass;
import hr.fer.opp.projekt.model.AuthorizationLevel;
import hr.fer.opp.projekt.model.BuildingType;
import hr.fer.opp.projekt.model.Image;
import hr.fer.opp.projekt.model.LandscapeView;
import hr.fer.opp.projekt.model.Reservation;
import hr.fer.opp.projekt.model.User;

public class ServletsUtility {
	private static final String GMAIL_USERNAME = "donotreply.jaganjci@gmail.com";

	private static final String GMAIL_PASSWORD = "krhencar";

	private static final String BY_CITY_STRING = "bycity";

	private static final String BY_COUNTRY_STRING = "bycountry";

	private static final String BY_SERVICES_STRING = "byservices";

	private static final BiFunction<Map<String, Integer>, Reservation, Integer> BY_CITY = (
			m, r) -> m.containsKey(r.getClient().getCity())
					? m.put(r.getClient().getCity(),
							m.get(r.getClient().getCity()) + 1)
					: m.put(r.getClient().getCity(), 1);

	private static final BiFunction<Map<String, Integer>, Reservation, Integer> BY_COUNTRY = (
			m, r) -> m.containsKey(r.getClient().getCountry())
					? m.put(r.getClient().getCountry(),
							m.get(r.getClient().getCountry()) + 1)
					: m.put(r.getClient().getCountry(), 1);

	/**
	 * 
	 * @param req
	 * @param resp
	 * @param levels
	 * @return {@code true} if the user isn't allowed on the page, {@code false}
	 *         otherwise.
	 * @throws IOException
	 * @throws ServletException
	 */
	public static boolean allowOnly(HttpServletRequest req,
			HttpServletResponse resp, AuthorizationLevel... levels)
			throws ServletException, IOException {
		User user = (User) req.getAttribute("user");
		if (user != null) {
			for (AuthorizationLevel level : levels) {
				if (user.getAuthorizationLevel().equals(level)) {
					return false;
				}
			}
		}

		req.setAttribute("errorMessage", "Forbidden.");
		req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
		return true;
	}

	public static void sendError(HttpServletRequest req,
			HttpServletResponse resp, String error)
			throws ServletException, IOException {
		req.setAttribute("errorMessage", error);
		req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
	}

	public static void sendInfo(HttpServletRequest req,
			HttpServletResponse resp, String title, String info)
			throws ServletException, IOException {
		req.setAttribute("title", title);
		req.setAttribute("info", info);
		req.getRequestDispatcher("/WEB-INF/pages/Info.jsp").forward(req, resp);
	}

	public static void sendEmail(String to, String subject, String body) {
		Runnable emailSend = new Runnable() {

			@Override
			public void run() {
				Properties props = new Properties();
				props.put("mail.smtp.auth", "true");
				props.put("mail.smtp.starttls.enable", "true");
				props.put("mail.smtp.host", "smtp.gmail.com");
				props.put("mail.smtp.port", "587");

				Session session = Session.getInstance(props,
						new javax.mail.Authenticator() {
							protected PasswordAuthentication getPasswordAuthentication() {
								return new PasswordAuthentication(
										GMAIL_USERNAME, GMAIL_PASSWORD);
							}
						});

				try {
					Message message = new MimeMessage(session);
					message.setFrom(new InternetAddress(GMAIL_USERNAME));
					message.setRecipients(Message.RecipientType.TO,
							InternetAddress.parse(to));
					message.setSubject(subject);
					message.setText(body);
					Transport.send(message);

				} catch (MessagingException e) {
				}
			}
		};
		Thread thread = new Thread(emailSend);
		thread.start();
	}

	public static void initializeAcommodationUnits(HttpServletRequest req) {
		initializeBuildingsAB("A", req);
		initializeBuildingsAB("B", req);
		initializeBuildingC(req);
		initializeBuildingD(req);
	}

	private static void initializeBuildingD(HttpServletRequest req) {
		ApartmentClass apartmentsSea = new ApartmentClass("D-more-a",
				BuildingType.D, 6, 8, LandscapeView.SEA,
				AcommodationType.APARTMENT,
				"Apartman zgrade D s pogledom na more.",
				getImages("D-park-a", req));
		ArrayList<AcommodationUnit> dApartmentsSea = new ArrayList<>();
		dApartmentsSea.add(new AcommodationUnit("D1-1", apartmentsSea));
		apartmentsSea.setUnits(dApartmentsSea);

		ApartmentClass apartmentsPark = new ApartmentClass("D-park-a",
				BuildingType.D, 6, 8, LandscapeView.PARK,
				AcommodationType.APARTMENT,
				"Apartman zgrade D s pogledom na park.",
				getImages("D-park-a", req));
		ArrayList<AcommodationUnit> dApartmentsPark = new ArrayList<>();
		dApartmentsPark.add(new AcommodationUnit("D1-2", apartmentsPark));
		apartmentsPark.setUnits(dApartmentsPark);

		ApartmentClass roomsSea = new ApartmentClass("D-more-s", BuildingType.D,
				2, 3, LandscapeView.SEA, AcommodationType.ROOM,
				"Soba zgrade D s pogledom na more.", getImages("D-more-s", req));
		ArrayList<AcommodationUnit> dRoomsSea = new ArrayList<>();
		dRoomsSea.add(new AcommodationUnit("D2-1", roomsSea));
		dRoomsSea.add(new AcommodationUnit("D2-2", roomsSea));
		dRoomsSea.add(new AcommodationUnit("D2-3", roomsSea));
		dRoomsSea.add(new AcommodationUnit("D2-4", roomsSea));
		roomsSea.setUnits(dRoomsSea);

		ApartmentClass roomsPark = new ApartmentClass("D-park-s",
				BuildingType.D, 2, 3, LandscapeView.PARK, AcommodationType.ROOM,
				"Soba zgrade D s pogledom na park.", getImages("D-park-s", req));
		ArrayList<AcommodationUnit> dRoomsPark = new ArrayList<>();
		dRoomsPark.add(new AcommodationUnit("D2-5", roomsPark));
		dRoomsPark.add(new AcommodationUnit("D2-6", roomsPark));
		dRoomsPark.add(new AcommodationUnit("D2-7", roomsPark));
		dRoomsPark.add(new AcommodationUnit("D2-8", roomsPark));
		roomsPark.setUnits(dRoomsPark);

		putInDatabase(apartmentsSea, apartmentsPark, roomsSea, roomsPark);
	}

	private static void initializeBuildingC(HttpServletRequest req) {
		ApartmentClass apartmentsSea = new ApartmentClass("C-more",
				BuildingType.C, 6, 8, LandscapeView.SEA,
				AcommodationType.APARTMENT,
				"Apartman zgrade C s pogledom na more.",
				getImages("C-more", req));
		ArrayList<AcommodationUnit> cApartmentsSea = new ArrayList<>();
		cApartmentsSea.add(new AcommodationUnit("C1-1", apartmentsSea));
		cApartmentsSea.add(new AcommodationUnit("C2-1", apartmentsSea));
		apartmentsSea.setUnits(cApartmentsSea);

		ApartmentClass apartmentsPark = new ApartmentClass("C-park",
				BuildingType.C, 6, 8, LandscapeView.PARK,
				AcommodationType.APARTMENT,
				"Apartman zgrade C s pogledom na park.",
				getImages("C-park", req));
		ArrayList<AcommodationUnit> cApartmentsPark = new ArrayList<>();
		cApartmentsPark.add(new AcommodationUnit("C1-2", apartmentsPark));
		cApartmentsPark.add(new AcommodationUnit("C2-2", apartmentsPark));
		apartmentsPark.setUnits(cApartmentsPark);

		putInDatabase(apartmentsSea, apartmentsPark);
	}

	private static void initializeBuildingsAB(String b,
			HttpServletRequest req) {
		ApartmentClass apartmentsSea = new ApartmentClass(b + "-more",
				b.equals("A") ? BuildingType.A : BuildingType.B, 2, 4,
				LandscapeView.SEA, AcommodationType.APARTMENT,
				"Apartman zgrade " + b + " s pogledom na more.",
				getImages(b + "-more", req));
		ArrayList<AcommodationUnit> bApartmentsSea = new ArrayList<>();
		bApartmentsSea.add(new AcommodationUnit(b + "1-1", apartmentsSea));
		bApartmentsSea.add(new AcommodationUnit(b + "1-2", apartmentsSea));
		bApartmentsSea.add(new AcommodationUnit(b + "2-1", apartmentsSea));
		bApartmentsSea.add(new AcommodationUnit(b + "2-2", apartmentsSea));
		apartmentsSea.setUnits(bApartmentsSea);

		ApartmentClass apartmentsPark = new ApartmentClass(b + "-park",
				b.equals("A") ? BuildingType.A : BuildingType.B, 2, 4,
				LandscapeView.PARK, AcommodationType.APARTMENT,
				"Apartman zgrade " + b + " s pogledom na park.",
				getImages(b + "-park", req));
		ArrayList<AcommodationUnit> bApartmentsPark = new ArrayList<>();
		bApartmentsPark.add(new AcommodationUnit(b + "1-3", apartmentsPark));
		bApartmentsPark.add(new AcommodationUnit(b + "1-4", apartmentsPark));
		bApartmentsPark.add(new AcommodationUnit(b + "2-3", apartmentsPark));
		bApartmentsPark.add(new AcommodationUnit(b + "2-4", apartmentsPark));
		apartmentsPark.setUnits(bApartmentsPark);

		putInDatabase(apartmentsSea, apartmentsPark);
	}

	private static void putInDatabase(ApartmentClass... apartments) {
		for (ApartmentClass apclass : apartments) {
			DAOProvider.getDAO().addObject(apclass);
		}
	}

	private static List<Image> getImages(String label, HttpServletRequest req) {

		List<Image> images = new ArrayList<>();
		String path = "src/main/webapp/WEB-INF/acommodationImages/";

		switch (Character.toUpperCase(label.charAt(0))) {

		case 'A':
			addImagesA(images, path, label);
			break;

		case 'B':
			addImagesB(images, path, label);
			break;

		case 'C':
			addImagesC(images, path, label);
			break;

		case 'D':
			addImagesD(images, path, label);
			break;

		default:
			break;
		}

		return images;
	}

	private static void addImagesA(List<Image> images, String path,
			String label) {
		try {
			images.add(new Image("jpg", "Unutrašnjost objekta A",
					(Files.readAllBytes(Paths.get(path + "A.jpg")))));

			if (label.equalsIgnoreCase("A-more")) {
				images.add(new Image("jpg", "Pogled na more objekta A",
						(Files.readAllBytes(Paths.get(path + "A-sea.jpg")))));

			} else {
				images.add(new Image("JPG", "Pogled na park objekta A",
						(Files.readAllBytes(Paths.get(path + "A-park.JPG")))));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void addImagesB(List<Image> images, String path,
			String label) {
		try {
			images.add(new Image("jpg", "Unutrašnjost objekta B",
					(Files.readAllBytes(Paths.get(path + "B.jpg")))));

			if (label.equalsIgnoreCase("B-more")) {
				images.add(new Image("jpg", "Pogled na more objekta B",
						(Files.readAllBytes(Paths.get(path + "B-sea.jpg")))));

			} else {
				images.add(new Image("jpg", "Pogled na park objekta B",
						(Files.readAllBytes(Paths.get(path + "B-park.jpg")))));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void addImagesC(List<Image> images, String path,
			String label) {
		try {
			images.add(new Image("jpeg", "Unutrašnjost objekta C",
					(Files.readAllBytes(Paths.get(path + "C.jpeg")))));

			if (label.equalsIgnoreCase("C-more")) {
				images.add(new Image("JPG", "Pogled na more objekta C",
						(Files.readAllBytes(Paths.get(path + "C-sea.JPG")))));

			} else {
				images.add(new Image("jpg", "Pogled na park objekta C",
						(Files.readAllBytes(Paths.get(path + "C-park.jpg")))));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void addImagesD(List<Image> images, String path,
			String label) {
		try {
			if (label.endsWith("a")) {
				images.add(new Image("jpeg", "Unutrašnjost apartmana objekta D",
						(Files.readAllBytes(Paths.get(path + "D1.jpeg")))));

				if (label.startsWith("D-more")) {
					images.add(new Image("jpg",
							"Pogled na more apartmana objekta D",
							(Files.readAllBytes(Paths.get(path + "D-sea.jpg")))));

				} else {
					images.add(new Image("jpg",
							"Pogled na park apartmana objekta D",
							(Files.readAllBytes(Paths.get(path + "D-park.jpg")))));
				}

			} else {
				images.add(new Image("jpg", "Unutrašnjost sobe objekta D",
						(Files.readAllBytes(Paths.get(path + "D2.jpg")))));

				if (label.startsWith("D-more")) {
					images.add(new Image("jpg", "Pogled na more sobe objekta D",
							(Files.readAllBytes(Paths.get(path + "D-sea.jpg")))));
				} else {
					images.add(new Image("jpg", "Pogled na park sobe objekta D",
							(Files.readAllBytes(Paths.get(path + "D-park.jpg")))));
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Map<String, Integer> getPieChartData(String criteria) {
		List<Reservation> reservations = DAOProvider.getDAO()
				.getAllReservations();

		switch (criteria.toLowerCase()) {

		case BY_CITY_STRING:
			return getReservationsByCriteria(reservations, BY_CITY);

		case BY_COUNTRY_STRING:
			return getReservationsByCriteria(reservations, BY_COUNTRY);

		case BY_SERVICES_STRING:
			return getByAdditionalServices(reservations);

		default:
			return null;
		}

	}

	private static Map<String, Integer> getReservationsByCriteria(
			List<Reservation> reservations,
			BiFunction<Map<String, Integer>, Reservation, Integer> function) {

		Map<String, Integer> map = new TreeMap<>();
		reservations.parallelStream().forEach(r -> function.apply(map, r));
		return map;
	}

	private static Map<String, Integer> getByAdditionalServices(
			List<Reservation> reservations) {
		Map<AdditionalServices, Integer> tmpMap = new HashMap<>();
		Map<String, Integer> map = new HashMap<>();

		reservations.parallelStream().forEach(r -> {
			r.getAdditionalServices().forEach(s -> {
				if (tmpMap.containsKey(s)) {
					tmpMap.put(s, tmpMap.get(s) + 1);
				} else {
					tmpMap.put(s, 1);
				}
			});
		});

		map.put("Bežićni internet", tmpMap.get(AdditionalServices.WIFI));
		map.put("Satelitska televizija",
				tmpMap.get(AdditionalServices.SATELLITE_TV));
		map.put("Parkirno mjesto",
				tmpMap.get(AdditionalServices.PARKING_SPACE));

		return map;
	}

}
