package hr.fer.opp.projekt.web.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import hr.fer.opp.projekt.dao.DAOException;
import hr.fer.opp.projekt.dao.DAOProvider;
import hr.fer.opp.projekt.model.User;

@WebFilter(filterName="CurrentUserFilter", urlPatterns="/*")
public class CurrentUserFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		Long userID = (Long) req.getSession().getAttribute("userID");
		User user = null;
		if (userID != null) {
			try {
				user = DAOProvider.getDAO().getUser(userID);
				req.setAttribute("user", user);
			} catch (DAOException e) {
				req.getSession().invalidate();
			}
		}
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
	}

}
