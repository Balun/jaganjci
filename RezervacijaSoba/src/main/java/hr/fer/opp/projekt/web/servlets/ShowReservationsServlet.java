package hr.fer.opp.projekt.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.opp.projekt.dao.DAOProvider;
import hr.fer.opp.projekt.model.AuthorizationLevel;
import hr.fer.opp.projekt.web.ServletsUtility;

@WebServlet("/servlets/admin/showReservations")
public class ShowReservationsServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if (ServletsUtility.allowOnly(req, resp, AuthorizationLevel.ADMIN)) {
			return;
		}
		
		req.setAttribute("reservations", DAOProvider.getDAO().getAllReservations());
		req.getRequestDispatcher("/WEB-INF/pages/ShowReservations.jsp").forward(req, resp);
	}
}
