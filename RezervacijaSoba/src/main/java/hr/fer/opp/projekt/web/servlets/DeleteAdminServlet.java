package hr.fer.opp.projekt.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.opp.projekt.dao.DAOException;
import hr.fer.opp.projekt.dao.DAOProvider;
import hr.fer.opp.projekt.model.AuthorizationLevel;
import hr.fer.opp.projekt.model.User;
import hr.fer.opp.projekt.web.ServletsUtility;

@WebServlet("/servlets/owner/deleteAdmin")
public class DeleteAdminServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		if (ServletsUtility.allowOnly(req, resp, AuthorizationLevel.OWNER)) {
			return;
		}

		String email = req.getParameter("email");
		if (email == null) {
			ServletsUtility.sendError(req, resp, "Nedostaje parametar \"email\".");
			return;
		}

		try {
			User admin = DAOProvider.getDAO().getUser(email);
			if (admin != null) {
				DAOProvider.getDAO().removeObject(admin);
				req.getRequestDispatcher(
						"/WEB-INF/pages/AdminRemovalConformation.jsp")
						.forward(req, resp);
			} else {
				ServletsUtility.sendError(req, resp,
						"Administrator ne postoji.");
				return;
			}
		} catch (DAOException e) {
			e.printStackTrace();
		}
	}

}
