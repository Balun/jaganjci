package hr.fer.opp.projekt.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.opp.projekt.model.AuthorizationLevel;
import hr.fer.opp.projekt.model.User;
import hr.fer.opp.projekt.web.ServletsUtility;
import hr.fer.opp.projekt.web.servlets.form.OwnerDataForm;

@WebServlet("/servlets/ownerDataInput")
public class OwnerDataServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		if (ServletsUtility.allowOnly(req, resp, AuthorizationLevel.OWNER)) {
			return;
		}
		
		if (req.getServletContext().getAttribute("ownerConfirmed") != null) {
			ServletsUtility.sendError(req, resp, "Vlasnički podaci su već unijeti.");
			return;
		}
		req.getRequestDispatcher("/WEB-INF/pages/OwnerDataInput.jsp")
				.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		if (ServletsUtility.allowOnly(req, resp, AuthorizationLevel.OWNER)) {
			return;
		}
		if (req.getServletContext().getAttribute("ownerConfirmed") != null) {
			ServletsUtility.sendError(req, resp, "Vlasnički podaci su već unijeti.");
			return;
		}

		OwnerDataForm form = new OwnerDataForm();
		form.fillFromHttpRequest(req);
		form.validate();
		req.setAttribute("form", form);

		if (!form.hasErrors()) {
			User owner = (User) req.getAttribute("user");
			owner.setEmail(form.getEmail());
			owner.setPhoneNumber(form.getPhoneNumber());
			owner.setConfirmedEmail(true);
			resp.sendRedirect(req.getServletContext().getContextPath()
					+ "/servlets/main");
		} else {
			req.getRequestDispatcher("/WEB-INF/pages/OwnerDataInput.jsp").forward(req, resp);
		}
	}
}
