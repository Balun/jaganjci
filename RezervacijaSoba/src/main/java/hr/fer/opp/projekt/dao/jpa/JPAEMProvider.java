package hr.fer.opp.projekt.dao.jpa;

import javax.persistence.EntityManager;

import hr.fer.opp.projekt.dao.DAOException;

/**
 * Class used for providing an {@link EntityManager} which can be used to access
 * the persistence layer. New entity manager is provided for each requesting
 * thread. Resource transaction is started upon requesting the entity manager
 * and is closed upon calling the {@link #close()} method.
 * 
 * @author Borna Zeba
 * @version 1.0
 *
 */
public class JPAEMProvider {

	/**
	 * Map of the entity managers mapped to their threads.
	 */
	private static ThreadLocal<LocalData> locals = new ThreadLocal<>();

	/**
	 * Returns an entity manager and starts the transaction.
	 * 
	 * @return entity manager for the calling thread
	 */
	public static EntityManager getEntityManager() {
		LocalData ldata = locals.get();
		if (ldata == null) {
			ldata = new LocalData();
			ldata.em = JPAEMFProvider.getEmf().createEntityManager();
			ldata.em.getTransaction().begin();
			locals.set(ldata);
		}
		return ldata.em;
	}

	/**
	 * Closes the entity manager for the calling thread and commits the
	 * transaction.
	 * 
	 * @throws DAOException
	 *             if error occurs while closing the entity manager or
	 *             committing a transaction
	 */
	public static void close() throws DAOException {
		LocalData ldata = locals.get();
		if (ldata == null) {
			return;
		}
		DAOException dex = null;
		try {
			ldata.em.getTransaction().commit();
		} catch (Exception ex) {
			dex = new DAOException("Unable to commit transaction.", ex);
		}
		try {
			ldata.em.close();
		} catch (Exception ex) {
			if (dex != null) {
				dex = new DAOException("Unable to close entity manager.", ex);
			}
		}
		locals.remove();
		if (dex != null)
			throw dex;
	}

	/**
	 * Class used for storing data local to a single thread.
	 * 
	 * @author Borna Zeba
	 * @version 1.0
	 *
	 */
	private static class LocalData {

		/**
		 * Entity manager.
		 */
		EntityManager em;
	}
}