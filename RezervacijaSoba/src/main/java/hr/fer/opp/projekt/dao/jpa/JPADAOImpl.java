package hr.fer.opp.projekt.dao.jpa;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import hr.fer.opp.projekt.dao.DAO;
import hr.fer.opp.projekt.dao.DAOException;
import hr.fer.opp.projekt.model.AcommodationUnit;
import hr.fer.opp.projekt.model.ApartmentClass;
import hr.fer.opp.projekt.model.AuthorizationLevel;
import hr.fer.opp.projekt.model.BuildingType;
import hr.fer.opp.projekt.model.Image;
import hr.fer.opp.projekt.model.Reservation;
import hr.fer.opp.projekt.model.User;

/**
 * Implementation of the {@link DAO} interface using Java Persistence API.
 * 
 * @author Borna Zeba
 * @version 1.0
 *
 */
@SuppressWarnings("unchecked")
public class JPADAOImpl implements DAO {

	@Override
	public void addObject(Object obj) throws DAOException {
		try {
			EntityManager em = JPAEMProvider.getEntityManager();
			em.persist(obj);
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public void removeObject(Object obj) throws DAOException {
		try {
			EntityManager em = JPAEMProvider.getEntityManager();
			em.remove(obj);
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public User getUser(String email) throws DAOException {
		try {
			EntityManager em = JPAEMProvider.getEntityManager();

			List<User> users = (List<User>) em.createNamedQuery("User.byEmail").setParameter("email", email)
					.getResultList();

			return users.size() == 0 ? null : users.get(0);
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public List<Reservation> getReservations(User client) throws DAOException {
		try {
			EntityManager em = JPAEMProvider.getEntityManager();
			return (List<Reservation>) em.createNamedQuery("Reservation.byClient").setParameter("id", client.getId())
					.getResultList();
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public User getUser(Long id) throws DAOException {
		try {
			EntityManager em = JPAEMProvider.getEntityManager();
			return em.find(User.class, id);
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public User getOwner() throws DAOException {
		try {

			EntityManager em = JPAEMProvider.getEntityManager();
			List<User> owner = (List<User>) em.createNamedQuery("User.owner").getResultList();

			if (!owner.isEmpty()) {
				return owner.get(0);
			} else {
				return null;
			}

		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public List<User> getUsers(AuthorizationLevel level) throws DAOException {
		try {

			EntityManager em = JPAEMProvider.getEntityManager();
			return (List<User>) em.createNamedQuery("User.byAuthorizationLevel")
					.setParameter("authorizationLevel", level).getResultList();

		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public List<ApartmentClass> getClassesFromBuilding(BuildingType building) throws DAOException {
		try {

			EntityManager em = JPAEMProvider.getEntityManager();
			return (List<ApartmentClass>) em.createNamedQuery("ApartmentClass.byBuilding")
					.setParameter("building", building).getResultList();

		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public ApartmentClass getClassByName(String apClass) throws DAOException {
		try {

			EntityManager em = JPAEMProvider.getEntityManager();
			List<ApartmentClass> apartments = (List<ApartmentClass>) em.createNamedQuery("ApartmentClass.byName")
					.setParameter("className", apClass).getResultList();

			if (!apartments.isEmpty()) {
				return apartments.get(0);

			} else {
				return null;
			}

		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public List<ApartmentClass> getClasses() throws DAOException {
		try {

			EntityManager em = JPAEMProvider.getEntityManager();
			List<ApartmentClass> apartments = (List<ApartmentClass>) em.createNamedQuery("ApartmentClass.all")
					.getResultList();

			if (!apartments.isEmpty()) {
				return apartments;

			} else {
				return null;
			}

		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public Image getImage(Long id) throws DAOException {
		try {
			EntityManager em = JPAEMProvider.getEntityManager();
			return em.find(Image.class, id);
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public List<AcommodationUnit> getAcommodationUnits() throws DAOException {
		try {
			EntityManager em = JPAEMProvider.getEntityManager();
			List<AcommodationUnit> units = (List<AcommodationUnit>) em.createNamedQuery("AcommodationUnit.all")
					.getResultList();

			if (!units.isEmpty()) {
				return units;

			} else {
				return null;
			}

		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public List<Reservation> getAllReservations() throws DAOException {
		try {
			EntityManager em = JPAEMProvider.getEntityManager();
			return (List<Reservation>) em.createNamedQuery("Reservation.all").getResultList();
		} catch (Exception e) {
			throw new DAOException();
		}
	}

	@Override
	public List<AcommodationUnit> getAcommodationUnits(ApartmentClass apClass) throws DAOException {
		try {
			EntityManager em = JPAEMProvider.getEntityManager();
			List<AcommodationUnit> units = (List<AcommodationUnit>) em.createNamedQuery("AcommodationUnit.byClass")
					.setParameter("apClass", apClass).getResultList();

			if (!units.isEmpty()) {
				return units;

			} else {
				return null;
			}

		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public Reservation getReservation(Long id) throws DAOException {
		try {
			EntityManager em = JPAEMProvider.getEntityManager();
			return em.find(Reservation.class, id);
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public AcommodationUnit getAcommodationUnit(String label) throws DAOException {
		try {
			EntityManager em = JPAEMProvider.getEntityManager();
			List<AcommodationUnit> units = (List<AcommodationUnit>) em.createNamedQuery("AcommodationUnit.byLabel")
					.setParameter("label", label).getResultList();

			if (!units.isEmpty()) {
				return units.get(0);

			} else {
				return null;
			}

		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public List<Reservation> getUnconfirmedReservations() throws DAOException {
		try {
			EntityManager em = JPAEMProvider.getEntityManager();
			return (List<Reservation>) em.createNamedQuery("Reservation.unconfirmed").setParameter("date", new Date())
					.getResultList();
		} catch (Exception e) {
			throw new DAOException();
		}
	}

	@Override
	public List<Reservation> getFutureReservations() throws DAOException {
		try {
			EntityManager em = JPAEMProvider.getEntityManager();
			return (List<Reservation>) em.createNamedQuery("Reservation.future").setParameter("date", new Date())
					.getResultList();
		} catch (Exception e) {
			throw new DAOException();
		}
	}

}
