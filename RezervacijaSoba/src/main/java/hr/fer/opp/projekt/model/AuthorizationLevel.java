package hr.fer.opp.projekt.model;

public enum AuthorizationLevel {
	OWNER, ADMIN, USER
}
