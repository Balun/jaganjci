package hr.fer.opp.projekt.dao;

import hr.fer.opp.projekt.dao.jpa.JPADAOImpl;

/**
 * Class which provides the currently used DAO.
 * 
 * @author Borna Zeba
 * @version 1.0
 *
 */
public class DAOProvider {

	/**
	 * DAO which will be used.
	 */
	private static DAO dao = new JPADAOImpl();

	/**
	 * Empty private constructor. Prevents instancing of this class.
	 */
	private DAOProvider() {
	}

	/**
	 * Returns the currently used DAO.
	 * 
	 * @return currently used DAO.
	 */
	public static DAO getDAO() {
		return dao;
	}

}
