package hr.fer.opp.projekt.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.opp.projekt.dao.DAOProvider;
import hr.fer.opp.projekt.model.AuthorizationLevel;
import hr.fer.opp.projekt.model.Reservation;
import hr.fer.opp.projekt.web.ServletsUtility;
import hr.fer.opp.projekt.web.servlets.form.ReservationForm;

@WebServlet("/servlets/admin/handleReservation")
public class ReservationHandlerServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		if (ServletsUtility.allowOnly(req, resp, AuthorizationLevel.ADMIN)) {
			return;
		}

		String action = req.getParameter("action");
		if (action == null) {
			ServletsUtility.sendError(req, resp, "Nije specificirana akcija.");
			return;
		}
		String idString = req.getParameter("id");
		if (idString == null) {
			ServletsUtility.sendError(req, resp, "Nije specificiran \"id\".");
			return;
		}
		Long id;
		try {
			id = Long.parseLong(idString);
		} catch (NumberFormatException e) {
			ServletsUtility.sendError(req, resp,
					"Parametar \"id\" mora biti broj.");
			return;
		}

		switch (action.toLowerCase()) {
		case "confirm":
			Reservation reservation = DAOProvider.getDAO().getReservation(id);
			reservation.setConfirmed(true);
			resp.sendRedirect(req.getServletContext().getContextPath() + "/servlets/admin/displayReservations");
			
			break;
		case "edit":
			resp.sendRedirect(req.getServletContext().getContextPath()
					+ "/servlets/admin/editReservation?id=" + idString);
			break;
		case "delete":
			synchronized (ReservationForm.syncObject) {
				reservation = DAOProvider.getDAO().getReservation(id);
				DAOProvider.getDAO().removeObject(reservation);
				resp.sendRedirect(req.getServletContext().getContextPath() + "/servlets/admin/displayReservations");
			}
			break;
		default:
			ServletsUtility.sendError(req, resp,
					"Definirana akcija ne postoji.");
			return;
		}
	}

}
