package hr.fer.opp.projekt.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.opp.projekt.dao.DAOProvider;
import hr.fer.opp.projekt.model.ApartmentClass;
import hr.fer.opp.projekt.model.AuthorizationLevel;
import hr.fer.opp.projekt.web.ServletsUtility;
import hr.fer.opp.projekt.web.servlets.form.ApartmentClassForm;

@MultipartConfig
@WebServlet("/servlets/owner/addApartmentClass")
public class AddApartmentClassServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if (ServletsUtility.allowOnly(req, resp, AuthorizationLevel.OWNER)) {
			return;
		}

		String regMethod = req.getParameter("method");
		if ("Cancel".equals(regMethod)) {
			resp.sendRedirect(req.getServletContext().getContextPath() + "/servlets/owner/editAcommodations");
			return;

		}

		ApartmentClassForm form = new ApartmentClassForm();
		form.fillFromHttpRequest(req);
		form.validate();

		req.setAttribute("regForm", form);

		if (!form.hasErrors()) {
			ApartmentClass apClass = new ApartmentClass();
			form.fillApartmentClassData(apClass);
			DAOProvider.getDAO().addObject(apClass);
			req.setAttribute("apClass", apClass.getClassName());
			req.setAttribute("message", "Uspješno unesena klasa " + apClass.getClassName() + ".");
			req.getRequestDispatcher("/WEB-INF/pages/AddAcommodationUnit.jsp").forward(req, resp);

		} else {
			req.getRequestDispatcher("/WEB-INF/pages/AddApartmentClass.jsp").forward(req, resp);
		}
	}
}
