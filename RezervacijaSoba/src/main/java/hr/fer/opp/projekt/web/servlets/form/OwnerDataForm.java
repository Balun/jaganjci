package hr.fer.opp.projekt.web.servlets.form;

import javax.servlet.http.HttpServletRequest;

public class OwnerDataForm extends AbstractForm {

	private String email;

	private String phoneNumber;

	public String getEmail() {
		return email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	@Override
	public void validate() {
		errors.clear();

		if (email.isEmpty()) {
			errors.put("email", "Morate upisati e-mail adresu.");
		} else if (!RegisterUserForm.isEmailValid(email)) {
			errors.put("email", "E-mail adresa nije ispravna.");
		} else if (phoneNumber.isEmpty()) {
			errors.put("phoneNumber", "Morate upisati broj telefona.");
		}
	}

	@Override
	public void fillFromHttpRequest(HttpServletRequest req) {
		this.email = prepare(req.getParameter("email"));
		this.phoneNumber = prepare(req.getParameter("phoneNumber"));
	}
}
