package hr.fer.opp.projekt.web.servlets;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.opp.projekt.dao.DAOProvider;
import hr.fer.opp.projekt.model.User;
import hr.fer.opp.projekt.web.Initialization;
import hr.fer.opp.projekt.web.ServletsUtility;

@WebServlet("/servlets/mailConfirmation")
public class MailConfirmationServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		@SuppressWarnings("unchecked")
		Map<String, Long> confirmationMap = (Map<String, Long>) req
				.getServletContext()
				.getAttribute(Initialization.EMAIL_CONFIRMATION_MAP);
		String confirmationString = req.getParameter("id");
		if (confirmationString == null
				|| !confirmationMap.containsKey(confirmationString)) {
			ServletsUtility.sendError(req, resp, "E-mail je već potvrđen.");
			return;
		}
		User user = DAOProvider.getDAO()
				.getUser(confirmationMap.get(confirmationString));
		if (user == null) {
			ServletsUtility.sendError(req, resp,
					"Ne postoji traženi korisnik za potvrdu.");
			return;
		}
		user.setConfirmedEmail(true);
		confirmationMap.remove(confirmationString);

		ServletsUtility.sendEmail(user.getEmail(), "Pristupni podaci korisnika",
				composeMessage(user));

		req.setAttribute("user", user);
		req.getRequestDispatcher("/WEB-INF/pages/ConfirmedMail.jsp")
				.forward(req, resp);
	}

	private String composeMessage(User user) {
		StringBuilder builder = new StringBuilder();

		builder.append("Ime korisnika: " + user.getFirstName() + "\n");
		builder.append("Prezime korisnika: " + user.getLastName() + "\n");
		builder.append("Adresa E-pošte: " + user.getEmail() + "\n");

		if (!user.getPhoneNumber().isEmpty()) {
			builder.append("Kontakt broj: " + user.getPhoneNumber() + "\n");
		}
		return builder.toString();
	}
}
