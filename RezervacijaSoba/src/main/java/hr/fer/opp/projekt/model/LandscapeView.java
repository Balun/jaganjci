package hr.fer.opp.projekt.model;

public enum LandscapeView {
	SEA("More"), PARK("Park");

	private String name;

	private LandscapeView(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}

	public static LandscapeView byName(String name) {
		switch (name.toLowerCase()) {
		case "more":
			return LandscapeView.SEA;

		case "park":
			return LandscapeView.PARK;

		default:
			throw new IllegalArgumentException();
		}
	}
}
