package hr.fer.opp.projekt.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "reservations")
@NamedQueries({
		@NamedQuery(name = "Reservation.byClient", query = "SELECT r FROM Reservation AS r WHERE r.client.id = :id"),
		@NamedQuery(name = "Reservation.byAcommodation", query = "SELECT r FROM Reservation AS r WHERE r.reservedUnit.id = :id"),
		@NamedQuery(name = "Reservation.all", query = "SELECT r FROM Reservation AS r"),
		@NamedQuery(name = "Reservation.unconfirmed", query = "SELECT r FROM Reservation AS r WHERE r.confirmed = false AND r.reservationStart > :date"),
		@NamedQuery(name = "Reservation.future", query = "SELECT r FROM Reservation AS r WHERE r.reservationStart > :date") })

public class Reservation {

	@Id
	@GeneratedValue
	private Long id;

	@ManyToOne
	@JoinColumn(nullable = false)
	private User client;

	@Temporal(TemporalType.DATE)
	@Column(nullable = false)
	private Date reservationStart;

	@Temporal(TemporalType.DATE)
	@Column(nullable = false)
	private Date reservationEnd;

	@ManyToOne
	@JoinColumn(nullable = false)
	private AcommodationUnit reservedUnit;

	@Column
	private int adults;

	@Column
	private int child_0_1;

	@Column
	private int child_2_7;

	@Column
	private int child_8_14;

	@ElementCollection(fetch = FetchType.LAZY)
	@Enumerated(EnumType.STRING)
	private List<AdditionalServices> additionalServices;

	@Column
	private boolean confirmed;

	public Reservation() {
		this.confirmed = false;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getClient() {
		return client;
	}

	public void setClient(User client) {
		this.client = client;
	}

	public Date getReservationStart() {
		return reservationStart;
	}

	public void setReservationStart(Date reservationStart) {
		this.reservationStart = reservationStart;
	}

	public Date getReservationEnd() {
		return reservationEnd;
	}

	public void setReservationEnd(Date reservationEnd) {
		this.reservationEnd = reservationEnd;
	}

	public AcommodationUnit getReservedUnit() {
		return reservedUnit;
	}

	public void setReservedUnit(AcommodationUnit reservedUnit) {
		this.reservedUnit = reservedUnit;
	}

	public int getAdults() {
		return adults;
	}

	public void setAdults(int adults) {
		this.adults = adults;
	}

	public int getChild_0_1() {
		return child_0_1;
	}

	public void setChild_0_1(int child_0_1) {
		this.child_0_1 = child_0_1;
	}

	public int getChild_2_7() {
		return child_2_7;
	}

	public void setChild_2_7(int child_2_7) {
		this.child_2_7 = child_2_7;
	}

	public int getChild_8_14() {
		return child_8_14;
	}

	public void setChild_8_14(int child_8_14) {
		this.child_8_14 = child_8_14;
	}

	public List<AdditionalServices> getAdditionalServices() {
		return additionalServices;
	}

	public void setAdditionalServices(List<AdditionalServices> additionalServices) {
		this.additionalServices = additionalServices;
	}

	public int getNumberOfResidents() {
		return adults + child_0_1 + child_2_7 + child_8_14;
	}

	public boolean isConfirmed() {
		return confirmed;
	}

	public void setConfirmed(boolean confirmed) {
		this.confirmed = confirmed;
		this.client.setConfirmedFirstReservation(true);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Reservation other = (Reservation) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
