package hr.fer.opp.projekt.web.servlets.form;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/**
 * Abstract class representing an HTML form. This class implements the error
 * handling methods, as each field can have it's own error message.
 * 
 * @author Borna Zeba
 * @version 1.0
 *
 */
public abstract class AbstractForm {

	/**
	 * Map of errors, where each error text is mapped to the name of the field
	 * it's referring to.
	 */
	protected Map<String, String> errors = new HashMap<>();

	/**
	 * Returns an error message for the field with the given name, or
	 * {@code null} if there is no error message.
	 * 
	 * @param field
	 *            name of the field
	 * @return error message assigned to the field with the given name
	 */
	public String getError(String field) {
		return errors.get(field);
	}

	/**
	 * Returns {@code true} if the whole form contains any errors, returns
	 * {@code false} otherwise.
	 * 
	 * @return {@code true} if the form contains any errors, {@code false} if
	 *         the form doesn't contain any error
	 */
	public boolean hasErrors() {
		return !errors.isEmpty();
	}

	/**
	 * Returns {@code true} if the field with the given name contains an error.
	 * 
	 * @param field
	 *            the name of the field
	 * @return {@code true} if the field with the given name contains an error,
	 *         {@code false} otherwise
	 */
	public boolean hasError(String field) {
		return errors.containsKey(field);
	}

	/**
	 * Helper method used for preparing the string to the form that is suitable
	 * to be displayed as the part of the web form. It prepares and returns the
	 * given parameter. The parameter is trimmed, and if the parameter is
	 * {@code null} an empty string is returned.
	 * 
	 * @param parameter
	 *            the parameter to prepare for the web form
	 * @return the prepared parameter
	 */
	protected String prepare(String parameter) {
		return parameter == null ? "" : parameter.trim();
	}
	
	protected String prepareWithoutTrim(String parameter) {
		return parameter == null ? "" : parameter;
	}


	/**
	 * Method validates all fields and generates the appropriate errors if such
	 * exist. Errors can be retrieved from the {@link #errors} map.
	 */
	public abstract void validate();

	/**
	 * Fills this form with the data from the given request.
	 * 
	 * @param req
	 *            http request containing the form data
	 */
	public abstract void fillFromHttpRequest(HttpServletRequest req);

}
