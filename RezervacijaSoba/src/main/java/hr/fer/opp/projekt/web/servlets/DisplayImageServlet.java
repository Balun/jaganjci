package hr.fer.opp.projekt.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.opp.projekt.dao.DAOProvider;
import hr.fer.opp.projekt.model.Image;
import hr.fer.opp.projekt.web.ServletsUtility;

@WebServlet("/servlets/image")
public class DisplayImageServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String id = req.getParameter("id");
		if (id == null) {
			ServletsUtility.sendError(req, resp, "Nedostaje parametar \"id\".");
			return;
		}

		Image image = null;
		try {
			image = DAOProvider.getDAO().getImage(Long.parseLong(id));
		} catch (NumberFormatException e) {
			ServletsUtility.sendError(req, resp,
					"Parametar \"id\" nije ispravan.");
			return;
		}
		if (image == null) {
			ServletsUtility.sendError(req, resp,
					"Ne postoji slika sa traženim ID-om.");
			return;
		}

		resp.setContentType("image/" + image.getMimeType());
		ServletOutputStream out = resp.getOutputStream();
		out.write(image.getImageFile());
		out.flush();
		out.close();
	}

}
