package hr.fer.opp.projekt.dao;

import java.util.List;

import hr.fer.opp.projekt.model.AcommodationUnit;
import hr.fer.opp.projekt.model.ApartmentClass;
import hr.fer.opp.projekt.model.AuthorizationLevel;
import hr.fer.opp.projekt.model.BuildingType;
import hr.fer.opp.projekt.model.Image;
import hr.fer.opp.projekt.model.Reservation;
import hr.fer.opp.projekt.model.User;

/**
 * Models data access object which enables accessing and managing the data.
 * 
 * @author Borna Zeba
 * @version 1.0
 *
 */
public interface DAO {

	/**
	 * Adds the given object to the data storage.
	 * 
	 * @param obj
	 *            object to be added
	 * @throws DAOException
	 *             if error occurs while adding the given data
	 */
	public void addObject(Object obj) throws DAOException;

	public void removeObject(Object obj) throws DAOException;

	public User getUser(Long id) throws DAOException;

	public User getUser(String email) throws DAOException;

	public User getOwner() throws DAOException;

	public List<User> getUsers(AuthorizationLevel level) throws DAOException;

	public List<Reservation> getReservations(User client) throws DAOException;

	public List<Reservation> getAllReservations() throws DAOException;

	public List<Reservation> getUnconfirmedReservations() throws DAOException;
	
	public List<Reservation> getFutureReservations() throws DAOException;

	public Reservation getReservation(Long id) throws DAOException;

	public List<ApartmentClass> getClassesFromBuilding(BuildingType type)
			throws DAOException;

	public ApartmentClass getClassByName(String apClass) throws DAOException;

	public List<AcommodationUnit> getAcommodationUnits() throws DAOException;

	public List<ApartmentClass> getClasses() throws DAOException;

	public AcommodationUnit getAcommodationUnit(String label)
			throws DAOException;

	public Image getImage(Long id) throws DAOException;

	public List<AcommodationUnit> getAcommodationUnits(ApartmentClass apClass)
			throws DAOException;
}
