package hr.fer.opp.projekt.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.opp.projekt.dao.DAOProvider;
import hr.fer.opp.projekt.model.User;
import hr.fer.opp.projekt.web.servlets.form.LoginForm;

/**
 * Servlet used for user login.
 * 
 * @author Borna Zeba
 * @version 1.0
 *
 */
@WebServlet("/servlets/login")
public class LoginServlet extends HttpServlet {

	/**
	 * Version ID.
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("/WEB-INF/pages/Login2.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		LoginForm loginForm = new LoginForm();
		loginForm.fillFromHttpRequest(req);
		loginForm.validate();

		req.setAttribute("userForm", loginForm);

		if (!loginForm.hasErrors()) {
			User user = DAOProvider.getDAO().getUser(req.getParameter("email"));
			req.getSession().setAttribute("userID", user.getId());
			resp.sendRedirect(req.getServletContext().getContextPath() + "/servlets/ponuda");
		} else {
			req.getRequestDispatcher("/WEB-INF/pages/Login2.jsp").forward(req, resp);
		}
	}
}
