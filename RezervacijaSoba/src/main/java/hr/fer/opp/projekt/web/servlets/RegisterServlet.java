package hr.fer.opp.projekt.web.servlets;

import java.io.IOException;
import java.security.SecureRandom;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.opp.projekt.dao.DAOProvider;
import hr.fer.opp.projekt.model.User;
import hr.fer.opp.projekt.web.Initialization;
import hr.fer.opp.projekt.web.ServletsUtility;
import hr.fer.opp.projekt.web.servlets.form.RegisterUserForm;

/**
 * Web serlvet used for managing the user registration.
 * 
 * @author Borna Zeba
 * @version 1.0
 *
 */
@WebServlet("/servlets/register")
public class RegisterServlet extends HttpServlet {

	/**
	 * Version ID.
	 */
	private static final long serialVersionUID = 1L;

	private static final char[] randomCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
			.toCharArray();

	private static final int RANDOM_STRING_LENGTH = 20;

	private static final String MAIL_SUBJECT = "Rezervacija soba - potvrda registracije";

	private static final String MAIL_BODY = "Poštovani,\nza potvrdu Vašeg računa pratite sljedeću poveznicu: %s";

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		String regMethod = req.getParameter("method");
		if (!"Submit".equals(regMethod)) {
			resp.sendRedirect(req.getServletContext().getContextPath() + "/");
			return;
		}

		RegisterUserForm registrationForm = new RegisterUserForm();
		registrationForm.fillFromHttpRequest(req);
		registrationForm.validate();

		req.setAttribute("regForm", registrationForm);

		if (!registrationForm.hasErrors()) {
			User newUser = new User();
			registrationForm.fillUserData(newUser);
			DAOProvider.getDAO().addObject(newUser);
			resp.sendRedirect(req.getServletContext().getContextPath() + "/servlets/main");
			sendCofirmationMail(req, newUser);
		} else {
			req.getRequestDispatcher("/WEB-INF/pages/Register.jsp").forward(req,
					resp);
		}
	}

	private void sendCofirmationMail(HttpServletRequest req, User user) {
		String randomString = generateRandomString();

		@SuppressWarnings("unchecked")
		Map<String, Long> confirmationMap = (Map<String, Long>) req
				.getServletContext()
				.getAttribute(Initialization.EMAIL_CONFIRMATION_MAP);
		confirmationMap.put(randomString, user.getId());
		String link = req.getScheme() + "://" + req.getServerName() + ":"
				+ req.getServerPort() + req.getContextPath()
				+ "/servlets/mailConfirmation?id=" + randomString;

		ServletsUtility.sendEmail(user.getEmail(), MAIL_SUBJECT,
				String.format(MAIL_BODY, link));
	}

	private String generateRandomString() {
		SecureRandom rand = new SecureRandom();
		char[] randomArray = new char[RANDOM_STRING_LENGTH];
		for (int i = 0; i < randomArray.length; i++) {
			randomArray[i] = randomCharacters[rand
					.nextInt(randomCharacters.length)];
		}
		return new String(randomArray);
	}

}
