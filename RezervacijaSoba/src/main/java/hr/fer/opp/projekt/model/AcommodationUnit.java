package hr.fer.opp.projekt.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "acommodation_units")
@NamedQueries({ @NamedQuery(name = "AcommodationUnit.all", query = "SELECT a FROM AcommodationUnit AS a"),
				@NamedQuery(name = "AcommodationUnit.byLabel", query = "SELECT a FROM AcommodationUnit AS a WHERE a.label = :label"),
				@NamedQuery(name = "AcommodationUnit.byClass", query = "SELECT a FROM AcommodationUnit AS a WHERE a.apartmentClass = :apClass")})
public class AcommodationUnit {

	@Id
	@GeneratedValue
	private Long id;

	@Column(length = 20, nullable = false, unique = true)
	private String label;

	@OneToMany(mappedBy = "reservedUnit", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST, orphanRemoval = true)
	private List<Reservation> reservations;
	
	@ManyToOne
	@JoinColumn(nullable = false)
	private ApartmentClass apartmentClass;

	public AcommodationUnit(String label, ApartmentClass apartmentClass) {
		super();
		this.label = label;
		this.apartmentClass = apartmentClass;
	}
	
	public AcommodationUnit() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public List<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}

	public ApartmentClass getApartmentClass() {
		return apartmentClass;
	}

	public void setApartmentClass(ApartmentClass apartmentClass) {
		this.apartmentClass = apartmentClass;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((label == null) ? 0 : label.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AcommodationUnit other = (AcommodationUnit) obj;
		if (label == null) {
			if (other.label != null)
				return false;
		} else if (!label.equals(other.label))
			return false;
		return true;
	}
	
	
}
