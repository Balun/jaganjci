package hr.fer.opp.projekt.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.opp.projekt.dao.DAOProvider;
import hr.fer.opp.projekt.model.AcommodationUnit;
import hr.fer.opp.projekt.model.ApartmentClass;
import hr.fer.opp.projekt.model.AuthorizationLevel;
import hr.fer.opp.projekt.web.ServletsUtility;

@WebServlet("/servlets/owner/addAcommodation")
public class AddAcommodationServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if (ServletsUtility.allowOnly(req, resp, AuthorizationLevel.OWNER)) {
			return;
		}

		String regMethod = req.getParameter("method");
		if ("Cancel".equals(regMethod)) {
			resp.sendRedirect(req.getServletContext().getContextPath() + "/servlets/owner/editAcommodations");
			return;
		}

		String apClassParam = req.getParameter("apClass");
		if (apClassParam == null) {
			ServletsUtility.sendError(req, resp, "Klasa mora biti specificirana!");
			return;
		}

		String label = req.getParameter("label");
		if (label == null || label.isEmpty()) {
			req.setAttribute("apClass", apClassParam);
			req.setAttribute("labelError", "Morate unijeti oznaku smještajne jedinice.");
			req.setAttribute("label", "");
			req.getRequestDispatcher("/WEB-INF/pages/AddAcommodationUnit.jsp").forward(req, resp);;
			return;

		} else {
			if (DAOProvider.getDAO().getAcommodationUnit(label) != null) {
				req.setAttribute("labelError", "Već postoji smještajna jedinica s tom oznakom.");
				req.setAttribute("label", label);
				req.setAttribute("apClass", apClassParam);
				req.getRequestDispatcher("/WEB-INF/pages/AddAcommodationUnit.jsp").forward(req, resp);
				return;
			}
			
			ApartmentClass apClass = DAOProvider.getDAO().getClassByName(apClassParam);
			if(apClass == null){
				ServletsUtility.sendError(req, resp, "Ne postoji navedena klasa.");
				return;
			}
			
			AcommodationUnit unit = new AcommodationUnit(label, apClass);
			DAOProvider.getDAO().addObject(unit);
			req.setAttribute("apClass", apClassParam);
			req.setAttribute("message", "Uspješno unesena jedinica " + label + ".");
			req.getRequestDispatcher("/WEB-INF/pages/AddAcommodationUnit.jsp").forward(req, resp);
		}
	}
}
