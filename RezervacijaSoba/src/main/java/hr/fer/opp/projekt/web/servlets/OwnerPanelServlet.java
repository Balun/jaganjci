package hr.fer.opp.projekt.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.opp.projekt.dao.DAOProvider;
import hr.fer.opp.projekt.model.AuthorizationLevel;
import hr.fer.opp.projekt.web.ServletsUtility;

@WebServlet({"/servlets/owner/controlPanel", "/servlets/owner"})
public class OwnerPanelServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		if (ServletsUtility.allowOnly(req, resp, AuthorizationLevel.OWNER)) {
			return;
		}

		if (DAOProvider.getDAO().getClasses() != null) {
			req.setAttribute("initial", true);

		} else {
			req.setAttribute("initial", false);
		}

		req.getRequestDispatcher("/WEB-INF/pages/OwnerControlPanel.jsp")
				.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		if (ServletsUtility.allowOnly(req, resp, AuthorizationLevel.OWNER)) {
			return;
		}

		if (DAOProvider.getDAO().getClasses() == null) {
			ServletsUtility.initializeAcommodationUnits(req);
		}
		resp.sendRedirect(
				req.getContextPath() + "/servlets/owner/controlPanel");
	}
}
