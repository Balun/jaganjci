package hr.fer.opp.projekt.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.opp.projekt.dao.DAOProvider;
import hr.fer.opp.projekt.model.AuthorizationLevel;
import hr.fer.opp.projekt.model.User;
import hr.fer.opp.projekt.web.ServletsUtility;
import hr.fer.opp.projekt.web.servlets.form.RegisterUserForm;

@WebServlet("/servlets/owner/editAdminInfo")
public class EditAdminInfoServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		if (ServletsUtility.allowOnly(req, resp, AuthorizationLevel.OWNER)) {
			return;
		}

		String email = req.getParameter("adminEmail");
		if (email == null) {
			ServletsUtility.sendError(req, resp,
					"Nedostaje parametar \"adminEmail\".");
			return;
		}

		User admin = DAOProvider.getDAO().getUser(email);
		if (admin == null) {
			ServletsUtility.sendError(req, resp,
					"Ne postoji korisnik sa traženim e-mailom.");
		}

		req.setAttribute("adminId", Long.toString(admin.getId()));
		RegisterUserForm registrationForm = new RegisterUserForm();
		registrationForm.fillFromUserData(admin);
		req.setAttribute("regForm", registrationForm);

		req.getRequestDispatcher("/WEB-INF/pages/EditAdminInfo.jsp")
				.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		if (ServletsUtility.allowOnly(req, resp, AuthorizationLevel.OWNER)) {
			return;
		}

		String regMethod = req.getParameter("method");

		if (!"Submit".equals(regMethod)) {
			resp.sendRedirect(req.getServletContext().getContextPath() + "/servlets/owner/editAdminInfo");
			return;
		}
		Long userId = null;
		try {
			userId = Long.parseLong(req.getParameter("id"));
		} catch (NumberFormatException e) {
			ServletsUtility.sendError(req, resp, "Parametar \"id\" nije broj.");
			return;
		}

		RegisterUserForm registrationForm = new RegisterUserForm();
		registrationForm.fillFromHttpRequest(req);
		registrationForm.validateEdit();

		req.setAttribute("regForm", registrationForm);
		req.setAttribute("id", req.getParameter("id"));

		if (!registrationForm.hasErrors()) {
			User admin = DAOProvider.getDAO().getUser(userId);
			registrationForm.fillUserData(admin);
			req.setAttribute("firstName", admin.getFirstName());
			req.setAttribute("lastName", admin.getLastName());

			req.getRequestDispatcher("/WEB-INF/pages/AdminEditConformation.jsp")
					.forward(req, resp);
		} else {
			req.getRequestDispatcher("/WEB-INF/pages/EditAdminInfo.jsp")
					.forward(req, resp);
		}
	}
}
