package hr.fer.opp.projekt.administration;

import java.util.Date;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import hr.fer.opp.projekt.dao.DAO;
import hr.fer.opp.projekt.dao.DAOProvider;
import hr.fer.opp.projekt.dao.jpa.JPAEMFProvider;
import hr.fer.opp.projekt.dao.jpa.JPAEMProvider;
import hr.fer.opp.projekt.model.AuthorizationLevel;
import hr.fer.opp.projekt.model.User;

public class OwnerRegistration {
	
	private static final String OWNER_FIRST_NAME = "Matej";
	
	private static final String OWNER_LAST_NAME = "Balun";
	
	private static final AuthorizationLevel AUTHORISATION_LEVEL = AuthorizationLevel.OWNER;
	
	private static final String CITY = "Zagreb";
	
	private static final String COUNTRY = "Hrvatska";
	
	private static final String PASSWORD = "123456";
	
	private static final String STREET_NAME = "Podravski put";
	
	private static final int STREET_NUMBER = 5;

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("baza.podataka.rezervacije");
		JPAEMFProvider.setEmf(emf);
		initialize();
		JPAEMProvider.close();
		emf.close();
	}

	private static void initialize() {
		DAO dao = DAOProvider.getDAO();
		User owner = new User();
		owner.setFirstName(OWNER_FIRST_NAME);
		owner.setLastName(OWNER_LAST_NAME);
		owner.setAuthorizationLevel(AUTHORISATION_LEVEL);
		owner.setCity(CITY);
		owner.setCountry(COUNTRY);
		owner.setEmail(" ");
		owner.setPassword(PASSWORD);
		owner.setStreetName(STREET_NAME);
		owner.setStreetNumber(STREET_NUMBER);
		owner.setRegistrationDate(new Date());
		dao.addObject(owner);
	}

}
