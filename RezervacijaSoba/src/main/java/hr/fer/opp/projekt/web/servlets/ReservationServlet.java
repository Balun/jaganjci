package hr.fer.opp.projekt.web.servlets;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.StringJoiner;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.opp.projekt.dao.DAOProvider;
import hr.fer.opp.projekt.model.AuthorizationLevel;
import hr.fer.opp.projekt.model.Reservation;
import hr.fer.opp.projekt.model.User;
import hr.fer.opp.projekt.web.ServletsUtility;
import hr.fer.opp.projekt.web.servlets.form.ReservationForm;

@WebServlet("/servlets/reservation")
public class ReservationServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String MAIL_SUBJECT = "Potvrda rezervacije smještaja";
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(
			"EEE, dd.MM.yyyy", new Locale("hr", "HR"));

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		if (ServletsUtility.allowOnly(req, resp, AuthorizationLevel.OWNER,
				AuthorizationLevel.ADMIN, AuthorizationLevel.USER)) {
			return;
		}

		String apClass = req.getParameter("apClass");
		if (apClass == null) {
			ServletsUtility.sendError(req, resp,
					"Nedostaje parametar \"apClass\".");
			return;
		}
		req.setAttribute("apClass", apClass);
		req.getRequestDispatcher("/WEB-INF/pages/Reservation.jsp").forward(req,
				resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		if (ServletsUtility.allowOnly(req, resp, AuthorizationLevel.OWNER,
				AuthorizationLevel.ADMIN, AuthorizationLevel.USER)) {
			return;
		}

		String regMethod = req.getParameter("method");
		if (!"Submit".equals(regMethod)) {
			resp.sendRedirect(req.getServletContext().getContextPath()
					+ "/servlets/ponuda");
			return;
		}

		ReservationForm form = new ReservationForm(
				(User) req.getAttribute("user"));
		form.fillFromHttpRequest(req);

		synchronized (ReservationForm.syncObject) {
			form.validate();

			req.setAttribute("regForm", form);
			req.setAttribute("apClass", form.getReservedClass());

			if (!form.hasErrors()) {
				Reservation res = new Reservation();
				form.fillReservationData(res);
				DAOProvider.getDAO().addObject(res);
				ServletsUtility.sendInfo(req, resp, "Status rezervacije",
						"Uspješno ste rezervirali smještajnu jedinicu.");
				ServletsUtility.sendEmail(res.getClient().getEmail(),
						MAIL_SUBJECT, generateMailBody(res));
			} else {
				req.getRequestDispatcher("/WEB-INF/pages/Reservation.jsp")
						.forward(req, resp);
			}
		}
	}

	private String generateMailBody(Reservation res) {
		StringBuilder builder = new StringBuilder();

		builder.append(
				"Potvrda vaše rezervacije smještaja u apartmanskom naselju \"Kod nas je najljepše\":\n\n");
		builder.append("Korisnik: " + res.getClient().getFirstName() + " "
				+ res.getClient().getLastName() + "\n");
		builder.append("Datum početka rezervacije: "
				+ DATE_FORMAT.format(res.getReservationStart()) + "\n");
		builder.append("Datum kraja rezervacije: "
				+ DATE_FORMAT.format(res.getReservationEnd()) + "\n");
		builder.append("Rezervirana smještajna jedinica: "
				+ res.getReservedUnit().getApartmentClass().getClassName()
				+ "\n");
		builder.append("Broj osoba: " + res.getNumberOfResidents() + "\n");

		StringJoiner joiner = new StringJoiner(", ");
		builder.append("Dodatne usluge: ");
		res.getAdditionalServices().forEach(s -> joiner.add(s.toString()));
		builder.append(joiner.toString());

		builder.append(
				"\n\nU roku od tri dana će Vas kontaktirati predstavnik turističkog naselja. Srdačan pozdrav!\n");

		return builder.toString();
	}

}
