package hr.fer.opp.projekt.web.servlets.form;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import org.apache.commons.io.IOUtils;

import hr.fer.opp.projekt.model.AcommodationType;
import hr.fer.opp.projekt.model.ApartmentClass;
import hr.fer.opp.projekt.model.BuildingType;
import hr.fer.opp.projekt.model.Image;
import hr.fer.opp.projekt.model.LandscapeView;

@MultipartConfig
public class ApartmentClassForm extends AbstractForm {

	private String building;

	private String apClass;

	private String minimumCapacity;

	private String maximumCapacity;

	private String unitView;

	private String type;

	private String description;

	private List<Part> imageParts;
	
	private String imageLoadError;

	@Override
	public void fillFromHttpRequest(HttpServletRequest req) {
		this.building = prepare(req.getParameter("building"));
		this.apClass = prepare(req.getParameter("apClass"));
		this.minimumCapacity = prepare(req.getParameter("minimumCapacity"));
		this.maximumCapacity = prepare(req.getParameter("maximumCapacity"));
		this.unitView = prepare(req.getParameter("unitView"));
		this.type = prepare(req.getParameter("type"));
		this.description = prepare(req.getParameter("description"));

		try {
			this.imageParts = req.getParts().stream().filter(part -> "images".equals(part.getName()))
					.collect(Collectors.toList());
		} catch (IOException | ServletException e) {
		}
	}

	public void fillApartmentClassData(ApartmentClass apClass) {

		apClass.setBuilding(BuildingType.valueOf(this.building));
		apClass.setClassName(this.apClass);
		apClass.setMinimumCapacity(Integer.parseInt(this.minimumCapacity));
		apClass.setMaximumCapacity(Integer.parseInt(this.maximumCapacity));
		apClass.setUnitView(LandscapeView.byName(this.unitView));
		apClass.setType(AcommodationType.byName(this.type));
		apClass.setDescription(this.description);

		List<Image> images = new ArrayList<>();

		imageParts.forEach(i -> {
			String name = Paths.get(i.getSubmittedFileName()).getFileName().toString();
			String extension = name.substring(name.lastIndexOf('.') + 1);
			try {
				images.add(new Image(extension, name, IOUtils.toByteArray(i.getInputStream())));
				imageLoadError = null;
			} catch (IOException e) {
				imageLoadError = "Pogreška pri dodavanju slika.";
			}
		});

		apClass.setImages(images);
	}

	@Override
	public void validate() {
		errors.clear();
		
		if(imageLoadError != null){
			errors.put("images", imageLoadError);
		}

		if (building.isEmpty()) {
			errors.put("building", "Morate unijeti ime zgrade.");
		}

		if (apClass.isEmpty()) {
			errors.put("apClass", "Morate unijeti oznaku klase.");
		}

		try {
			if (Integer.parseInt(minimumCapacity) < 1) {
				errors.put("minimumCapacity", "Minimalni broj osoba ne može biti nula ili negativan!");
			}
		} catch (NumberFormatException e) {
			errors.put("minimumCapacity", "Pogrešan unos broja!");
		}

		try {
			if (Integer.parseInt(maximumCapacity) < 1) {
				errors.put("maximumCapacity", "Maksimalni broj osoba ne može biti = ili negativan!");
			}
		} catch (NumberFormatException e) {
			errors.put("maximumCapacity", "Pogrešan unos broja!");
		}

		if (!errors.containsKey("maximumCapacity") && !errors.containsKey("minimumCapacity")
				&& (Integer.parseInt(maximumCapacity) < Integer.parseInt(minimumCapacity))) {
			errors.put("maximumCapacity", "Maksimalni kapacitet ne može biti manji od minimalnog.");
		}

		if (unitView.isEmpty()) {
			errors.put("unitView", "Morate unijeti tip pogleda jedinice!");
		}

		try {
			LandscapeView.byName(unitView);
		} catch (IllegalArgumentException ex) {
			errors.put("unitView", "Pogled smještajne jedinice nije korektno unesen! (Unesite \"park\" ili \"more\")");
		}

		if (type.isEmpty()) {
			errors.put("type", "Morate unijeti tip jedinice!");
		}

		try {
			AcommodationType.byName(type);
		} catch (IllegalArgumentException ex) {
			errors.put("type", "Tip smještajne jedinice nije korektno unesen! (Unesite \"apartman\" ili \"soba\")");
		}
	}

	public String getBuilding() {
		return building;
	}

	public String getMinimumCapacity() {
		return minimumCapacity;
	}

	public String getMaximumCapacity() {
		return maximumCapacity;
	}

	public String getUnitView() {
		return unitView;
	}

	public String getDescription() {
		return description;
	}

	public List<Part> getImageParts() {
		return imageParts;
	}

	public String getApClass() {
		return apClass;
	}

	public String getType() {
		return type;
	}

}
