package hr.fer.opp.projekt.web.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import hr.fer.opp.projekt.dao.DAOProvider;
import hr.fer.opp.projekt.model.ApartmentClass;
import hr.fer.opp.projekt.model.BuildingType;
import hr.fer.opp.projekt.model.Image;
import hr.fer.opp.projekt.web.ServletsUtility;

@WebServlet("/servlets/offer")
public class OfferServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8484081421543996188L;

	private static final Gson GSON = new GsonBuilder()
			.setExclusionStrategies(new AcommodationUnitExclusion()).create();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String building = req.getParameter("object"); // npr. A
		if (building == null) {
			ServletsUtility.sendError(req, resp,
					"Nedostaje parametar \"object\".");
			return;
		}

		BuildingType buildingType = getApartmentType(building);
		if (buildingType == null) {
			ServletsUtility.sendError(req, resp, "Nevažeći parametri.");
			return;
		}

		List<ApartmentClass> apartments = DAOProvider.getDAO()
				.getClassesFromBuilding(getApartmentType(building));
		ApartmentClass[] array = new ApartmentClass[apartments.size()];

		String jsonList = GSON.toJson(apartments.toArray(array));

		resp.setContentType("application/json;charset=UTF-8");
		resp.getWriter().write(jsonList);
		resp.getWriter().flush();
	}

	private BuildingType getApartmentType(String building) {
		switch (building.toLowerCase()) {

		case "a":
			return BuildingType.A;

		case "b":
			return BuildingType.B;

		case "c":
			return BuildingType.C;

		case "d":
			return BuildingType.D;

		default:
			return null;
		}
	}

	public static class AcommodationUnitExclusion implements ExclusionStrategy {

		@Override
		public boolean shouldSkipClass(Class<?> arg0) {
			return false;
		}

		@Override
		public boolean shouldSkipField(FieldAttributes arg0) {
			if ((arg0.getDeclaringClass() == ApartmentClass.class)
					&& arg0.getName().equals("units")) {
				return true;
			}

			if ((arg0.getDeclaringClass() == Image.class)
					&& arg0.getName().equals("imageFile")) {
				return true;
			}
			return false;
		}

	}
}
