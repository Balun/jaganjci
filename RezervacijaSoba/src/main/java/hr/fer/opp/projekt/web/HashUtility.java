package hr.fer.opp.projekt.web;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

/**
 * Utility class containing the method which returns the {@code SHA-256} hash of
 * the given password as a hexadecimal string.
 * 
 * @author Borna Zeba
 * @version 1.0
 *
 */
public class HashUtility {
	

	/**
	 * Private empty constructor disabling instancing of this class.
	 */
	private HashUtility() {
	}

	/**
	 * Returns a hexadecimal representation of the given password hashed with
	 * the {@code SHA-256} algorithm.
	 * 
	 * @param password
	 *            the password to hash
	 * @return hashed password
	 */
	public static String hashValueOf(String password) {
		String hash = null;
		MessageDigest md = null;

		try {
			md = MessageDigest.getInstance("SHA-256");
			md.reset();
			md.update(password.getBytes("UTF-8"));
			hash = DatatypeConverter.printHexBinary(md.digest());

		} catch (NoSuchAlgorithmException
				| UnsupportedEncodingException ignorable) {
		}
		return hash;
	}
	
}
