package hr.fer.opp.projekt.dao.jpa;

import javax.persistence.EntityManagerFactory;

/**
 * Provider class used for fetching the instance of {@link EntityManagerFactory}
 * class.
 * 
 * @author Borna Zeba
 * @version 1.0
 *
 */
public class JPAEMFProvider {

	/**
	 * Entity manager factory which this class provides.
	 */
	private static EntityManagerFactory emf;

	/**
	 * Private constructor disabling the ability to instance this class.
	 */
	private JPAEMFProvider() {
	}

	/**
	 * Returns the instance of the {@link EntityManagerFactory}.
	 * 
	 * @return instance of the {@link EntityManagerFactory}.
	 */
	public static EntityManagerFactory getEmf() {
		return emf;
	}

	/**
	 * Sets the {@link EntityManagerFactory} which this class will provide.
	 * 
	 * @param emf
	 *            {@link EntityManagerFactory} to set.
	 */
	public static void setEmf(EntityManagerFactory emf) {
		JPAEMFProvider.emf = emf;
	}
}