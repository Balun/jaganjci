package hr.fer.opp.projekt.model;

public enum AcommodationType {
	APARTMENT("Apartman"), ROOM("Soba");

	private String name;

	private AcommodationType(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return this.name;
	}

	public static AcommodationType byName(String name) {
		switch (name.toLowerCase()) {

		case "apartman":
			return AcommodationType.APARTMENT;

		case "soba":
			return AcommodationType.ROOM;

		default:
			throw new IllegalArgumentException();
		}
	}
}
