package hr.fer.opp.projekt.web.servlets;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.opp.projekt.dao.DAOProvider;
import hr.fer.opp.projekt.model.AcommodationUnit;
import hr.fer.opp.projekt.model.AuthorizationLevel;
import hr.fer.opp.projekt.web.ServletsUtility;

@WebServlet("/servlets/owner/statistics")
public class RangListServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 39384121484402317L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if (ServletsUtility.allowOnly(req, resp, AuthorizationLevel.OWNER)) {
			return;
		}

		if (DAOProvider.getDAO().getAllReservations().isEmpty()) {
			ServletsUtility.sendError(req, resp, "Nisu uneseni potrebni podaci o rezervacijama za prikaz statistike.");
			return;
		}

		List<AcommodationUnit> units = DAOProvider.getDAO().getAcommodationUnits();
		units = units.parallelStream().filter(u -> !u.getReservations().isEmpty()).collect(Collectors.toList());
		req.setAttribute("units", units);

		req.getRequestDispatcher("/WEB-INF/pages/Statistics.jsp").forward(req, resp);
		;
	}
}
