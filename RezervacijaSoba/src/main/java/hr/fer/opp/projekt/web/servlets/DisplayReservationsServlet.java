package hr.fer.opp.projekt.web.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.opp.projekt.dao.DAOProvider;
import hr.fer.opp.projekt.model.AuthorizationLevel;
import hr.fer.opp.projekt.model.Reservation;
import hr.fer.opp.projekt.web.ServletsUtility;

@WebServlet("/servlets/admin/displayReservations")
public class DisplayReservationsServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		if (ServletsUtility.allowOnly(req, resp, AuthorizationLevel.ADMIN)) {
			return;
		}
		
		String filter = req.getParameter("filter");
		List<Reservation> reservations;
		if (filter != null && filter.equalsIgnoreCase("unconfirmed")) {
			reservations = DAOProvider.getDAO().getUnconfirmedReservations();
		} else {
			reservations = DAOProvider.getDAO().getAllReservations();
		}
		req.setAttribute("reservations", reservations);
		req.getRequestDispatcher("/WEB-INF/pages/ShowReservations.jsp").forward(req, resp);
	}
}
