package hr.fer.opp.projekt.web.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.opp.projekt.dao.DAOProvider;
import hr.fer.opp.projekt.model.User;

@WebFilter(filterName="OwnerExistenceFilter", urlPatterns="/*")
public class OwnerExistenceFilter implements Filter {

	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1,
			FilterChain arg2) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) arg0;

		Boolean ownerConfirmed = (Boolean) req.getServletContext()
				.getAttribute("ownerConfirmed");

		if (ownerConfirmed == null) {
			User owner = DAOProvider.getDAO().getOwner();
			if (owner != null) {
				if (owner.isConfirmedEmail()) {
					req.getServletContext().setAttribute("ownerConfirmed",
							new Boolean(true));
					arg2.doFilter(arg0, arg1);

				} else if (req.getRequestURI()
						.startsWith("/rezervacija/servlets/ownerLogin")
						|| req.getRequestURI().startsWith(
								"/rezervacija/servlets/ownerDataInput")) {
					arg2.doFilter(arg0, arg1);
				} else {
					((HttpServletResponse) arg1).sendRedirect(
							req.getContextPath() + "/servlets/ownerLogin");
				}

			} else {
				req.setAttribute("errorMessage", "No owner in database.");
				req.getRequestDispatcher("/WEB-INF/pages/Error.jsp")
						.forward(req, arg1);
			}

		} else {
			arg2.doFilter(arg0, arg1);
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

}
