package hr.fer.opp.projekt.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/servlets/error")
public class ErrorServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String errorMessage = req.getParameter("error");
		
		if (errorMessage != null) {
			req.setAttribute("errorMessage", errorMessage);
		} else {
			req.setAttribute("errorMessage", "Došlo je do pogreške.");
		}
		req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
	}
	
}
