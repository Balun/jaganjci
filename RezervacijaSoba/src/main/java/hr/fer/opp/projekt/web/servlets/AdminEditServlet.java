package hr.fer.opp.projekt.web.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.opp.projekt.dao.DAOException;
import hr.fer.opp.projekt.dao.DAOProvider;
import hr.fer.opp.projekt.model.AuthorizationLevel;
import hr.fer.opp.projekt.model.User;
import hr.fer.opp.projekt.web.ServletsUtility;

@WebServlet("/servlets/owner/editAdmins")
public class AdminEditServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if (ServletsUtility.allowOnly(req, resp, AuthorizationLevel.OWNER)) {
			return;
		}

		try {
			List<User> admins = DAOProvider.getDAO().getUsers(AuthorizationLevel.ADMIN);
			req.setAttribute("admins", admins);
			req.getRequestDispatcher("/WEB-INF/pages/EditAdmins.jsp").forward(req, resp);
		} catch (DAOException e) {
			e.printStackTrace();
		}
	}
}
