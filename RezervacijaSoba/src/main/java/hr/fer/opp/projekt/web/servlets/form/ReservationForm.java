package hr.fer.opp.projekt.web.servlets.form;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import hr.fer.opp.projekt.dao.DAOProvider;
import hr.fer.opp.projekt.model.AcommodationUnit;
import hr.fer.opp.projekt.model.AdditionalServices;
import hr.fer.opp.projekt.model.ApartmentClass;
import hr.fer.opp.projekt.model.AuthorizationLevel;
import hr.fer.opp.projekt.model.Reservation;
import hr.fer.opp.projekt.model.User;

public class ReservationForm extends AbstractForm {

	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

	private static final SimpleDateFormat ERROR_DATE_FORMAT = new SimpleDateFormat("dd.MM");

	private static final Date APARTMENTS_OPEN;

	private static final Date APARTMENTS_CLOSE;

	private static final int APARTMENTS_OPEN_DAY = 1;

	private static final int APARTMENTS_OPEN_MONTH = 5;

	private static final int APARTMENTS_CLOSE_DAY = 30;

	private static final int APARTMENTS_CLOSE_MONTH = 9;

	public static Object syncObject = new Object();

	private String reservationStart;

	private String reservationEnd;

	private String reservedClass;

	private String adults;

	private String child_0_1;

	private String child_2_7;

	private String child_8_14;

	private boolean parkingSpace;

	private boolean sateliteTv;

	private boolean wifi;

	private User client;

	private AcommodationUnit reservedUnit;

	private String oldId;

	public static final Comparator<Reservation> dateComparator = new Comparator<Reservation>() {

		@Override
		public int compare(Reservation o1, Reservation o2) {
			return o1.getReservationStart().compareTo(o2.getReservationStart());
		}
	};

	static {
		Calendar cal = Calendar.getInstance();
		cal.set(2000, APARTMENTS_OPEN_MONTH - 1, APARTMENTS_OPEN_DAY);
		APARTMENTS_OPEN = cal.getTime();
		cal.set(2000, APARTMENTS_CLOSE_MONTH - 1, APARTMENTS_CLOSE_DAY);
		APARTMENTS_CLOSE = cal.getTime();
	}

	public ReservationForm(User client) {
		super();
		this.client = client;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void validate() {
		errors.clear();

		if (reservationStart.isEmpty()) {
			errors.put("reservationStart", "Morate unijeti datum rezervacije!");
		}

		Date startDate = null;
		try {
			startDate = DATE_FORMAT.parse(reservationStart);
		} catch (ParseException e) {
			errors.put("reservationStart", "Krivi format datuma!");
		}

		if (reservationEnd.isEmpty()) {
			errors.put("reservationEnd", "Morate unijeti datum kraja rezervacije!");
		}

		Date endDate = null;
		try {
			endDate = DATE_FORMAT.parse(reservationEnd);
		} catch (ParseException e) {
			errors.put("reservationEnd", "Krivi format datuma!");
		}

		if (startDate != null && endDate != null) {

			if (endDate.before(startDate)) {
				errors.put("reservationEnd", "Kraj rezervacije mora biti nakon početka.");
			}

			Calendar reservationDelay = Calendar.getInstance();
			reservationDelay.add(Calendar.DAY_OF_MONTH, 4);
			if (startDate.before(reservationDelay.getTime())) {
				errors.put("reservationStart", "Morate izvršiti rezervaciju barem 4 dana ranije.");
			}

			if (getDaysDifference(startDate, endDate) == 0) {
				errors.put("reservationStart", "Morate rezervirati barem jedno noćenje.");
			}

			if (startDate.before(new Date(startDate.getYear(), APARTMENTS_OPEN_MONTH - 1, APARTMENTS_OPEN_DAY))) {
				errors.put("reservationStart",
						"Smještajni kapaciteti su otvoreni od " + ERROR_DATE_FORMAT.format(APARTMENTS_OPEN) + ".");
			}
			if (endDate.after(new Date(endDate.getYear(), APARTMENTS_CLOSE_MONTH - 1, APARTMENTS_CLOSE_DAY))) {
				errors.put("reservationEnd",
						"Smještajni kapaciteti su zatvoreni od " + ERROR_DATE_FORMAT.format(APARTMENTS_CLOSE) + ".");
			}
		}

		ApartmentClass apClass = DAOProvider.getDAO().getClassByName(reservedClass);

		int adultsNumber = 0;
		int child_0_1_Number = 0;
		int child_2_7_Number = 0;
		int child_8_14_Number = 0;

		try {
			adultsNumber = Integer.parseInt(adults);
			if (adultsNumber < 0) {
				errors.put("adultsNumber", "Ne može biti manje od 0.");
			}
		} catch (NumberFormatException e) {
			errors.put("adultsNumber", "Pogrešan unos, potrebno je unijeti broj.");
		}

		try {
			child_0_1_Number = Integer.parseInt(child_0_1);
			if (child_0_1_Number < 0) {
				errors.put("child_0_1", "Ne može biti manje od 0.");
			}
		} catch (NumberFormatException e) {
			errors.put("child_0_1", "Pogrešan unos, potrebno je unijeti broj.");
		}

		try {
			child_2_7_Number = Integer.parseInt(child_2_7);
			if (child_2_7_Number < 0) {
				errors.put("child_2_7", "Ne može biti manje od 0.");
			}
		} catch (NumberFormatException e) {
			errors.put("child_2_7", "Pogrešan unos, potrebno je unijeti broj.");
		}

		try {
			child_8_14_Number = Integer.parseInt(child_8_14);
			if (child_8_14_Number < 0) {
				errors.put("child_8_14", "Ne može biti manje od 0.");
			}
		} catch (NumberFormatException e) {
			errors.put("child_8_14", "Pogrešan unos, potrebno je unijeti broj.");
		}

		int count = adultsNumber + child_0_1_Number + child_2_7_Number + child_8_14_Number;

		if (count < apClass.getMinimumCapacity()) {
			errors.put("adultsNumber", "Ukupni broj osoba mora biti barem " + apClass.getMinimumCapacity() + "!");
		}

		if (count > apClass.getMaximumCapacity()) {
			errors.put("adultsNumber", "Ukupni broj osoba ne smije biti veći od " + apClass.getMaximumCapacity() + "!");
		}

		if (errors.isEmpty()) {
			if ((reservedUnit = optimiseReservation(apClass, startDate, endDate)) == null) {
				errors.put("reservationStart",
						"Smještajni kapaciteti nisu dostupni u odabranom terminu. Molimo odaberite drugi termin.");
			}
		}
	}

	@Override
	public void fillFromHttpRequest(HttpServletRequest req) {
		this.reservationStart = prepare(req.getParameter("reservationStart"));
		this.reservationEnd = prepare(req.getParameter("reservationEnd"));
		this.reservedClass = prepare(req.getParameter("apClass"));
		this.adults = prepare(req.getParameter("adultsNumber"));
		this.child_0_1 = prepare(req.getParameter("child_0_1"));
		this.child_2_7 = prepare(req.getParameter("child_2_7"));
		this.child_8_14 = prepare(req.getParameter("child_8_14"));
		this.parkingSpace = req.getParameter("parkingSpace") == null ? false : true;
		this.sateliteTv = req.getParameter("sateliteTv") == null ? false : true;
		this.wifi = req.getParameter("wifi") == null ? false : true;

		this.adults = adults.isEmpty() ? "0" : this.adults;
		this.child_0_1 = child_0_1.isEmpty() ? "0" : this.child_0_1;
		this.child_2_7 = child_2_7.isEmpty() ? "0" : this.child_2_7;
		this.child_8_14 = child_8_14.isEmpty() ? "0" : this.child_8_14;
	}

	public void fillReservationData(Reservation res) {
		res.setClient(client);
		try {
			res.setReservationStart(DATE_FORMAT.parse(reservationStart));
			res.setReservationEnd(DATE_FORMAT.parse(reservationEnd));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		res.setReservedUnit(reservedUnit);
		addApartmentMembers(res);
		addAdditionalServices(res);
		if (client.isConfirmedFirstReservation() || client.getAuthorizationLevel().equals(AuthorizationLevel.ADMIN)
				|| client.equals(AuthorizationLevel.OWNER)) {
			res.setConfirmed(true);
		}
	}

	public void fillFromReservationData(Reservation res) {
		this.reservationStart = res.getReservationStart().toString();
		this.reservationEnd = res.getReservationEnd().toString();
		this.reservedClass = res.getReservedUnit().getApartmentClass().getClassName();
		this.adults = Integer.toString(res.getAdults());
		this.child_0_1 = Integer.toString(res.getChild_0_1());
		this.child_2_7 = Integer.toString(res.getChild_2_7());
		this.child_8_14 = Integer.toString(res.getChild_8_14());
		this.parkingSpace = res.getAdditionalServices().contains(AdditionalServices.PARKING_SPACE);
		this.sateliteTv = res.getAdditionalServices().contains(AdditionalServices.SATELLITE_TV);
		this.wifi = res.getAdditionalServices().contains(AdditionalServices.WIFI);
		this.client = res.getClient();
		this.reservedUnit = res.getReservedUnit();
	}

	private void addAdditionalServices(Reservation res) {
		List<AdditionalServices> services = new ArrayList<>();

		if (parkingSpace) {
			services.add(AdditionalServices.PARKING_SPACE);
		}

		if (wifi) {
			services.add(AdditionalServices.WIFI);
		}

		if (sateliteTv) {
			services.add(AdditionalServices.SATELLITE_TV);
		}

		res.setAdditionalServices(services);
	}

	private void addApartmentMembers(Reservation res) {
		res.setAdults(adults.isEmpty() ? 0 : Integer.parseInt(adults));
		res.setChild_0_1(child_0_1.isEmpty() ? 0 : Integer.parseInt(child_0_1));
		res.setChild_2_7(child_2_7.isEmpty() ? 0 : Integer.parseInt(child_2_7));
		res.setChild_8_14(child_8_14.isEmpty() ? 0 : Integer.parseInt(child_8_14));
	}

	public String getReservationStart() {
		return reservationStart;
	}

	public String getReservationEnd() {
		return reservationEnd;
	}

	public String getReservedClass() {
		return reservedClass;
	}

	public String getAdultsNumber() {
		return adults;
	}

	public String getChild_0_1() {
		return child_0_1;
	}

	public String getChild_2_7() {
		return child_2_7;
	}

	public String getChild_8_14() {
		return child_8_14;
	}

	public boolean isParkingSpace() {
		return parkingSpace;
	}

	public boolean isSateliteTv() {
		return sateliteTv;
	}

	public boolean isWifi() {
		return wifi;
	}

	public AcommodationUnit getReservedUnit() {
		return reservedUnit;
	}

	public String getOldId() {
		return oldId;
	}

	public void setOldId(String oldId) {
		this.oldId = oldId;
	}

	private AcommodationUnit optimiseReservation(ApartmentClass apClass, Date reservationStart, Date reservatonEnd) {
		List<AcommodationUnit> units = DAOProvider.getDAO().getAcommodationUnits(apClass);

		int bestHole = Integer.MAX_VALUE;
		AcommodationUnit bestUnit = null;

		for (AcommodationUnit unit : units) {
			List<Reservation> reservations = unit.getReservations();
			if (reservations.isEmpty()) {
				return unit;
			}

			Collections.sort(reservations, dateComparator);
			reservations.parallelStream().filter(r -> r.getReservationEnd().after(new Date()))
					.collect(Collectors.toList());

			int reservationBefore = -2;
			for (int i = 0; i < reservations.size(); i++) {
				if (reservations.get(i).getReservationStart().after(reservationStart)) {
					reservationBefore = i - 1;
					break;
				}
			}
			if (reservationBefore == -2) {
				reservationBefore = reservations.size() - 1;
			}
			int hole = 0;
			if (reservationBefore >= 0) {
				if (reservations.get(reservationBefore).getReservationEnd().after(reservationStart)
						|| reservations.get(reservationBefore).getReservationStart().equals(reservationStart)) {
					continue;
				}
				hole = getDaysDifference(reservationStart, reservations.get(reservationBefore).getReservationEnd());
			} else if (reservations.get(0).getReservationStart().equals(reservationStart)) {
				continue;
			}

			if (reservationBefore != reservations.size() - 1) {
				Reservation reservationAfter = reservations.get(reservationBefore + 1);
				if (reservationAfter.getReservationStart().before(reservatonEnd)) {
					continue;
				}
				hole += getDaysDifference(reservatonEnd, reservationAfter.getReservationStart());
			}
			if (hole < bestHole) {
				bestHole = hole;
				bestUnit = unit;
			}
		}
		return bestUnit;
	}

	private int getDaysDifference(Date date1, Date date2) {
		long diff = date1.getTime() - date2.getTime();
		diff = Math.abs(diff);
		return (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}

}
