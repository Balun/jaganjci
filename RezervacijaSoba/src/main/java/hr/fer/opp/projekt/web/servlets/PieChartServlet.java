package hr.fer.opp.projekt.web.servlets;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.image.RenderedImage;
import java.io.IOException;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;

import hr.fer.opp.projekt.web.ServletsUtility;

@WebServlet("/servlets/owner/chart")
public class PieChartServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String criteria = req.getParameter("criteria");

		Map<String, Integer> data = ServletsUtility.getPieChartData(criteria);

		resp.setContentType("image/png");
		ServletOutputStream os = resp.getOutputStream();

		JFreeChart chart = getChart(data, criteria);

		RenderedImage chartImage = chart.createBufferedImage(300, 300);
		ImageIO.write(chartImage, "png", os);
		os.flush();
		os.close();

	}

	private JFreeChart getChart(Map<String, Integer> data, String criteria) {
		DefaultPieDataset dataset = new DefaultPieDataset();
		data.entrySet().forEach(e -> dataset.setValue(e.getKey(), e.getValue()));

		boolean legend = true;
		boolean tooltips = false;
		boolean urls = false;

		JFreeChart chart = ChartFactory.createPieChart(getTitle(criteria), dataset, legend, tooltips, urls);
		chart.setBorderPaint(Color.BLACK);
		chart.setBorderStroke(new BasicStroke(5.0f));
		chart.setBorderVisible(true);

		return chart;
	}

	private String getTitle(String criteria) {
		switch (criteria.toLowerCase()) {

		case "byservices":
			return "Najviše tražene dodatne usluge";

		case "bycity":
			return "Pregled rezervacija po gradovima";

		case "bycountry":
			return "Pregled rezervacija po državama";

		default:
			return "";
		}
	}

}
