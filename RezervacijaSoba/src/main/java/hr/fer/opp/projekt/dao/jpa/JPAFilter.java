package hr.fer.opp.projekt.dao.jpa;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

/**
 * Web filter which closes the connection to the JPA when servlet completes its
 * job. If servlet never requested a connection to the JPA (i.e. entity
 * manager), nothing will be closed.
 * 
 * @author Borna Zeba
 * @version 1.0
 *
 */
@WebFilter(filterName="BazaFilter", urlPatterns="/servlets/*")
public class JPAFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		try {
			chain.doFilter(request, response);
		} finally {
			JPAEMProvider.close();
		}
	}

	@Override
	public void destroy() {
	}

}