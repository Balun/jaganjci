package hr.fer.opp.projekt.web;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import hr.fer.opp.projekt.dao.jpa.JPAEMFProvider;

/**
 * Web listener which activates on servlet startup and shutdown and initializes
 * the persistence layer.
 * 
 * @author Borna Zeba
 * @version 1.0
 *
 */
@WebListener
public class Initialization implements ServletContextListener {

	/**
	 * Name of the persistence unit.
	 */
	private static final String PERSISTENCE_UNIT_NAME = "baza.podataka.rezervacije";

	/**
	 * Name of the attribute in the servlet context map which contains an entity
	 * manager factory.
	 */
	public static final String EM_FACTORY_ATTRIBUTE = "my.application.emf";

	public static final String EMAIL_CONFIRMATION_MAP = "email.map";

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		sce.getServletContext().setAttribute(EM_FACTORY_ATTRIBUTE, emf);
		Map<String, Long> emailMap = new HashMap<>();
		sce.getServletContext().setAttribute(EMAIL_CONFIRMATION_MAP, emailMap);
		JPAEMFProvider.setEmf(emf);
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		JPAEMFProvider.setEmf(null);
		EntityManagerFactory emf = (EntityManagerFactory) sce
				.getServletContext().getAttribute(EM_FACTORY_ATTRIBUTE);
		if (emf != null) {
			emf.close();
		}
	}
}
