package hr.fer.opp.projekt.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.opp.projekt.dao.DAOProvider;
import hr.fer.opp.projekt.model.AuthorizationLevel;
import hr.fer.opp.projekt.model.User;
import hr.fer.opp.projekt.web.ServletsUtility;
import hr.fer.opp.projekt.web.servlets.form.RegisterUserForm;

@WebServlet("/servlets/owner/adminRegistration")
public class AdminRegistrationServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final int MAX_ADMINS = 3;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		if (ServletsUtility.allowOnly(req, resp, AuthorizationLevel.OWNER)) {
			return;
		}
		if (DAOProvider.getDAO().getUsers(AuthorizationLevel.ADMIN)
				.size() >= MAX_ADMINS) {
			ServletsUtility.sendError(req, resp,
					"Već je registriran maksimalan broj administratora ("
							+ MAX_ADMINS + ").");
			return;
		}
		req.getRequestDispatcher("/WEB-INF/pages/AdminRegistration.jsp")
				.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		if (ServletsUtility.allowOnly(req, resp, AuthorizationLevel.OWNER)) {
			return;
		}
		if (DAOProvider.getDAO().getUsers(AuthorizationLevel.ADMIN)
				.size() >= MAX_ADMINS) {
			ServletsUtility.sendError(req, resp,
					"Već je registriran maksimalan broj administratora ("
							+ MAX_ADMINS + ").");
			return;
		}

		String regMethod = req.getParameter("method");

		if (!"Submit".equals(regMethod)) {
			resp.sendRedirect(req.getServletContext().getContextPath()
					+ "/servlets/owner/controlPanel");
			return;
		}

		RegisterUserForm registrationForm = new RegisterUserForm();
		registrationForm.fillFromHttpRequest(req);
		registrationForm.validate();

		req.setAttribute("regForm", registrationForm);

		if (!registrationForm.hasErrors()) {
			User admin = new User();
			admin.setAuthorizationLevel(AuthorizationLevel.ADMIN);
			registrationForm.fillUserData(admin);
			admin.setConfirmedEmail(true);
			DAOProvider.getDAO().addObject(admin);
			req.setAttribute("admin", admin);
			req.getRequestDispatcher("/WEB-INF/pages/AdminConformation.jsp")
					.forward(req, resp);
		} else {
			req.getRequestDispatcher("/WEB-INF/pages/AdminRegistration.jsp")
					.forward(req, resp);
		}
	}
}
