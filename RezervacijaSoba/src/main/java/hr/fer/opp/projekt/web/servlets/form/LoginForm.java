package hr.fer.opp.projekt.web.servlets.form;

import javax.servlet.http.HttpServletRequest;

import hr.fer.opp.projekt.dao.DAOProvider;
import hr.fer.opp.projekt.model.User;

/**
 * Class representing the form used for login.
 * 
 * @author Borna Zeba
 * @version 1.0
 *
 */
public class LoginForm extends AbstractForm {

	/**
	 * User's email.
	 */
	private String email;

	/**
	 * User's password.
	 */
	private String password;

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	@Override
	public void validate() {
		errors.clear();

		if (email.isEmpty()) {
			errors.put("email", "Morate upisati e-mail adresu.");
		} else if (password.isEmpty()) {
			errors.put("password", "Morate upisati lozinku.");
		} else {
			User user = DAOProvider.getDAO().getUser(email);
			if (user == null || !user.validatePassword(password)) {
				errors.put("password",
						"Ne postoji korisnik s unesenim podacima.");
			} else if (!user.isConfirmedEmail()) {
				errors.put("password",
						"Molimo potvrdite vašu e-mail adresu prije prijave.");
			}
		}
	}

	@Override
	public void fillFromHttpRequest(HttpServletRequest req) {
		this.email = prepare(req.getParameter("email"));
		this.password = prepareWithoutTrim(req.getParameter("password"));
	}
}
