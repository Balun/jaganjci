package hr.fer.opp.projekt;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import hr.fer.opp.projekt.dao.DAO;
import hr.fer.opp.projekt.dao.DAOProvider;
import hr.fer.opp.projekt.dao.jpa.JPAEMFProvider;
import hr.fer.opp.projekt.dao.jpa.JPAEMProvider;
import hr.fer.opp.projekt.model.AcommodationUnit;
import hr.fer.opp.projekt.model.ApartmentClass;
import hr.fer.opp.projekt.model.AuthorizationLevel;
import hr.fer.opp.projekt.model.BuildingType;
import hr.fer.opp.projekt.model.User;

public class InitializationTest {

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("baza.podataka.rezervacije");
		JPAEMFProvider.setEmf(emf);
		// initialize();
		//test();
		System.out.println(BuildingType.A);
		JPAEMProvider.close();
		emf.close();
	}

	@SuppressWarnings("unused")
	private static void test() {
		DAO dao = DAOProvider.getDAO();
		ApartmentClass clas = dao.getClassByName("A-more");
		List<AcommodationUnit> unit = dao.getAcommodationUnits(clas);
		System.out.println(unit.size());
	}

	@SuppressWarnings({ "unused" })
	private static void initialize() {
		DAO dao = DAOProvider.getDAO();
		User owner = new User();
		owner.setFirstName("Pero");
		owner.setLastName("Perić");
		owner.setAuthorizationLevel(AuthorizationLevel.OWNER);
		owner.setCity("Zagreb");
		owner.setCountry("Croatia");
		owner.setEmail("pero@peric.com");
		owner.setPassword("password");
		owner.setStreetName("Ilica");
		owner.setStreetNumber(100);
		owner.setRegistrationDate(new Date());
		dao.addObject(owner);

//		AcommodationUnit unit = new AcommodationUnit();
//		unit.setBuilding("A");
//		unit.setLabel("1-1");
//		unit.setMaximumCapacity(4);
//		unit.setMinimumCapacity(2);
//		unit.setUnitView(LandscapeView.SEA);
//		dao.addObject(unit);
//
//		Reservation reservation = new Reservation();
//		List<AgeGroup> members = new ArrayList<>();
//		members.add(AgeGroup.ADULT);
//		reservation.setApartmentMembers(members);
//		reservation.setApartmentMembers(members);
//		reservation.setClient(owner);
//		reservation.setReservationEnd(new Date(2016, 8, 28));
//		reservation.setReservationStart(new Date(2016, 8, 20));
//		reservation.setReservedUnit(unit);
//		dao.addObject(reservation);
//
//		User user = dao.getUser("pero@peric.com");
//		System.out.println(dao.getReservations(unit).get(0).getReservationStart());
//		System.out.println(dao.getAcommodationUnits().get(0).getLabel());
	}

}
