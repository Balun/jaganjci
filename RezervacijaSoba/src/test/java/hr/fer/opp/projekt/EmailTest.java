package hr.fer.opp.projekt;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailTest {
	private static final String MAIL_BODY = "Poštovani,\nza potvrdu Vašeg računa pratite sljedeću poveznicu: <a href=\"%s\">%s</a>";

	public static void main(String[] args) {
		String link = "http://www.google.hr";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication("donotreply.jaganjci@gmail.com",
								"krhencar");
					}
				});

		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("donotreply.jaganjci@gmail.com"));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse("matej.balun7@gmail.com"));
			message.setSubject("Test");
			message.setText(String.format(MAIL_BODY, link, link));
			message.addHeader("Content-Type", "text/html; charset=UTF-8");
			Transport.send(message);

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
}
